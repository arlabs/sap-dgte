$(function() {

    //region GENERAL
    // =======================================================================================
    var modalTitle = $('.modal-title');
    var modalSmall = $('.modal-small');
    var modalContentSmall = $('.modal-content-small');

    // Disable button on form submit
    $('form.on-submit-disable').on('submit', function(e) {
        $(this).find('button[type=submit], input[type=submit]').addClass('disabled');
    });
    $('a.on-click-disable').on('click', function() {
        if ($(this).hasClass('btn'))
        {
            $(this).addClass('disabled');
        }
    });

    // Delete package
    $('.btn-generic-delete').on('click', function(e) {
        var $this = $(this);
        var id = $this.data('id');
        var url = $this.data('url');

        addLoadingState($this,null,'Delete',true);

        $.ajax({
            url: 'delete',
            type: 'GET',
            data: {id: id, url: url}
        })
            .always(function(result) {
                modalTitle.html('Delete');
                modalContentSmall.html(result);
                modalSmall.modal('show');

                addLoadingState($this,'times','Delete',false);
            });

        e.preventDefault();
    });

    // Upload image
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

        $('.btn-file').addClass('disabled');

        $('.message').removeClass('text-danger').html('<i class="fa fa-spin fa-spinner"></i>');
    });

    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        var form = $(this).closest('form');

        var formData = new FormData();
        formData.append('file', $(this).prop('files')[0]);
        formData.append('_token', form.find('input[name=_token]').val());
        formData.append('path', form.find('input[name=path]').val());
        formData.append('id', form.find('input[name=id]').val() || null);

        var url = form.attr('action');
        var msg = $('.message');

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false
        })
            .always(function(result) {
                console.log(result);
                if (result.success)
                {
                    msg.removeClass('text-danger');
                    msg.addClass('text-success').html(result.message);

                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
                else
                {
                    msg.removeClass('text-success');
                    msg.addClass('text-danger').html(result.message);
                }

                $('.btn-file').removeClass('disabled');
            });
    });

    //endregion

    //region SERVICE PACKAGE
    // =======================================================================================

    // Add description field
    var btnAddDescription = $('.btn-add-description');
    btnAddDescription.unbind('click').on('click', function(e) {
        addDescriptionField($(this));

        e.preventDefault();
    });

    // Remove description field
    $('.box-body').unbind('click').on('click', '.btn-remove-description', function(e) {
        removeDescriptionField($(this));

        e.preventDefault();
    });

    // Check for empty description fields
    $('.form-package').on('submit', function(e) {
        var required = $(this).find('input.required');
        var submitBtn = $(this).find('button[type=submit]');
        var isEmpty = false;

        $.each(required, function() {
            if ( ! $(this).val())
            {
                isEmpty = true;
            }
        });

        if (isEmpty)
        {
            $('.error-msg').html('Fields are required!');

            submitBtn.removeClass('disabled');

            e.preventDefault();
        }
    });

    // If main package is selected, show package price and hide description price
    // If additional package is selected, hide package price and show description price
    var category = $('select[name=category]');
    category.unbind('change').on('change', function() {
        var descPrice = $('input.desc_price');
        var packagePrice = $('input#package_price').closest('.form-group');

        if ($(this).val() === 'main')
        {
            descPrice.hide();
            packagePrice.show();
        }
        else
        {
            descPrice.show();
            packagePrice.hide();
        }
    });

    // On edit page, hide desc_price if main package.
    var descPrice = $('input.desc_price');
    var packagePrice = $('input#package_price').closest('.form-group');
    if (category.val() === 'main')
    {
        descPrice.hide();
        packagePrice.show();
    }
    else
    {
        descPrice.show();
        packagePrice.hide();
    }

    //endregion

    //region BOOKING
    // =======================================================================================

    // Clone main package
    $('input[type=radio]').unbind('ifChecked').on('ifChecked', function(event){
        triggerCloning($(this));
    });

    var packageElem = $('.package');
    packageElem.on('click', function(e) {
        triggerCloning($(this));
    });

    // Add selected descriptions to My Booking
    var btnAddSelection = $('.btn-add-selection');
    btnAddSelection.unbind('click').on('click', function(e) {
        // Set package id
        $('input#cloned-additional-package-id').val($('.source-additional-package-id').data('package-id'));

        // Set package name
        $('#cloned-additional-package-name').html('Additional');

        // Remove all traces of iCheck
        $('input[type=checkbox]').iCheck('destroy');

        // Clone descriptions
        cloneDescription($(this),'additional');

        // Disable continue button if no event date
        var event_date = $('.form-continue').find('input[name=event_date]');
        if ( ! event_date.val())
        {
            continueButton('disable');
        }
        else
        {
            continueButton('enable');
        }
    });

    // Remove unchecked boxes
    var btnRemoveUnchecked = $('.btn-remove-unchecked');
    btnRemoveUnchecked.unbind('click').on('click', function() {
        removeUncheckedBoxes();

        // Disable continue button if no event date
        var event_date = $(this).closest('.form-continue').find('input[name=event_date]').val();
        ( ! event_date) ? continueButton('disable') : continueButton('enable');
    });

    // Check if there are selected main package descriptions
    $('.form-continue').on('submit', function(e) {
        var isEmpty = false;
        var holder = $('#cloned-main-descriptions');
        var source = holder.find('.source-descriptions');

        $.each(source, function() {
            var $this = $(this);

            var chk = $this.find('input');

            if (chk.prop('checked') == false)
            {
                isEmpty = true;
            }
        });

        if (isEmpty)
        {
            $('.error-msg').html('Remove unchecked boxes.');

            continueButton('disable');

            e.preventDefault();
        }
    });

    // Check date availability
    $('.form-check-date').on('submit', function(e) {
        e.preventDefault();

        var eventDate = $(this).find('input[name=event_date]').val();
        var hours = $(this).find('input[name=hours]').val();
        var btn = $(this).find('[type=submit]');
        var msg = $(this).find('.message');
        var eventHolder = $('input.event-date-holder');

        // Add loading state
        addLoadingState(btn,'check','Check Availability',true);

        // Check if event date is empty
        if ( ! eventDate)
        {
            msg.html('Please input a date.');

            // Remove loading state
            addLoadingState(btn,'check','Check Availability', false);

            return false;
        }

        $.ajax({
            url: $(this).attr('action'),
            type: "POST",
            data: {event_date: eventDate, hours: hours}
        })
            .always(function(result) {
                if (btn.hasClass('btn-default'))
                {
                    btn.removeClass('btn-default');
                    btn.addClass('btn-' + result.alert_level)
                }
                else
                {
                    btn.removeClass('btn-success');
                    btn.addClass('btn-' + result.alert_level)
                }

                msg.addClass('text-' + result.alert_level);
                msg.html(result.message);

                eventHolder.val(result.event_date);

                (result.is_date_available) ? continueButton('enable') : continueButton('disable');

                // Remove loading state
                addLoadingState(btn,'check','Check Availability', false);
            });
    });

    $('.table-datatable').DataTable({
        "sDom": '<"top"f>t<"bottom"p><"clear">'
    });

    //endregion

    //region COMMENTS
    // =======================================================================================

    // Store comment
    $('#form-comment').on('submit', function(e) {
        e.preventDefault();

        var token = $(this).find('[name=_token]');
        var url = $(this).attr('action');
        var method = $(this).attr('method');
        var content = $(this).find('[name=content]');
        var message = $(this).find('span.message');
        var btn = $(this).find('button[type=submit]');

        btn.addClass('disabled').html('<i class="fa fa-spin fa-spinner"></i> Comment');

        if ( ! content.val())
        {
            message.html('Please provide a comment.');

            btn.removeClass('disabled').html('<i class="fa fa-comment"></i> Comment');

            return false;
        }

        $.ajax({
            url: url,
            type: method,
            data: {_token: token.val(), content: content.val()}
        })
            .always(function(result) {
                btn.removeClass('disabled').html('<i class="fa fa-comment"></i> Comment');

                content.val('');

                message.html(result.message);
            });
    });

    //endregion
});

function triggerCloning($this)
{
    // Set package id
    $('input#cloned-main-package-id').val($this.data('package-id'));

    // Set package name
    $('#cloned-main-package-name').html($this.data('package-name'));

    // Check the source checkboxes
    var inputs = $this.closest('.form-group').find('input[type=checkbox]');
    inputs.prop('checked',true);

    // Remove all traces of iCheck
    $('input[type=checkbox]').iCheck('destroy');

    // Clone descriptions
    cloneDescription($this,'main');

    // Uncheck the source checkboxes
    inputs.prop('checked',false);

    // Hide cart cover
    $('#cart-cover').hide();

    // Add number of hours to My Booking Cart
    var hours = $this.closest('.form-group').find('input[name=hours]').val();
    $('.form-check-date').find('input[name=hours]').val(hours);

    // Check if user have inputted date
    var event_date = $('.form-continue').find('input[name=event_date]').val();
    ( ! event_date) ? continueButton('disable') : continueButton('enable');
}

function cloneDescription(elem,type)
{
    var sourceDescriptions = elem.closest('.box').find('.source-descriptions');

    // Clone
    var clonedDescriptions = sourceDescriptions.clone();

    // Remove hidden class
    clonedDescriptions.removeClass('hidden');

    $('#cloned-' + type + '-descriptions').html(clonedDescriptions);

    // Re-initialize iCheck
    $('input[type=checkbox]').iCheck({
        checkboxClass: 'icheckbox_square-red',
        radioClass: 'iradio_square-red',
        increaseArea: '20%' // optional
    });

    // Remove unchecked checkboxes
    removeUncheckedBoxes();
}

function removeUncheckedBoxes()
{
    var main = $('#cloned-main-descriptions');
    var additional = $('#cloned-additional-descriptions');

    removeMe([main,additional]);

    removeIdIfNoDescription();
}

function removeMe(holder)
{
    $.each(holder, function() {
        var source = $(this).find('.source-descriptions');
        $.each(source, function() {
            var $this = $(this);

            var chk = $this.find('input');

            if (chk.prop('checked') == false)
            {
                $this.remove();
            }
        });
    });
}

function removeIdIfNoDescription()
{
    // Main
    var mainChks = $('#cloned-main-descriptions').find('input');
    if ( ! mainChks.val())
    {
        $('input[name=main_package_id]').val('');

        continueButton('disable');

        $('#cloned-main-package-name').html('');

        $('#cart-cover').show();
    }
    else
    {
        continueButton('enable');

        $('#cart-cover').hide();
    }

    // Additional
    var additionalChks = $('#cloned-additional-descriptions').find('input');
    if ( ! additionalChks.val())
    {
        $('input[name=additional_package_id]').val('');

        $('#cloned-additional-package-name').html('');
    }
}

function continueButton(action)
{
    if (action === 'disable') {
        $('.btn-continue').addClass('disabled').attr('disabled');
    }
    else
    {
        $('.btn-continue').removeClass('disabled').removeAttr('disabled');
    }
}

function addDescriptionField($this)
{
    var row = $this.parents('.box-body').find('#source-clone');

    // Clone a row
    var cloned = row.clone();

    // Remove id from cloned
    cloned.removeAttr('id');

    // Clear the fields
    var inputs = cloned.find('input');
    inputs.val('');

    // Add default value (0) to price
    var descPrice = cloned.find('input.desc_price');
    descPrice.val('0');

    // Replace the button classes
    var clonedBtn = cloned.find('button');
    clonedBtn.removeClass('btn-add-description').addClass('btn-remove-description');
    clonedBtn.removeClass('btn-success').addClass('btn-danger');

    // Replace the button title attribute
    clonedBtn.removeAttr('title');
    clonedBtn.attr('title','Remove field');

    // Replace the icon
    var clonedIcon = clonedBtn.find('i');
    clonedIcon.removeClass('fa-plus').addClass('fa-times');

    // Place cloned row next to its source row
    $('.box-body div.row:last').after(cloned);
}

function removeDescriptionField($this)
{
    $this.closest('.row').remove();
}

function addLoadingState(elem,icon,html,disable)
{
    if (disable) {
        elem.html('<i class="fa fa-spin fa-spinner"></i> ' + html);
        elem.addClass('disabled').attr('disabled');
    }
    else
    {
        elem.html('<i class="fa fa-' + icon + '"></i> ' + html);
        elem.removeClass('disabled').removeAttr('disabled');
    }
}