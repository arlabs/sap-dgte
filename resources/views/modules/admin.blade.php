<li class="{{ Active::pattern(['users','users/*']) }}">
    <a href="{{ url('users') }}">
        Users
    </a>
</li>
<li class="{{ Active::pattern(['comments','comments/*']) }}">
    <a href="{{ url('comments') }}">
        Comments
    </a>
</li>
<li class="{{ Active::pattern(['locations','locations/*']) }}">
    <a href="{{ url('locations') }}">
        Locations
    </a>
</li>