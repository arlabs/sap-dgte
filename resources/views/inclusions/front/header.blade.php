<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <img src="/images/logo.png" class="img-responsive img-rounded" width="150" style="margin-top: -11px;"/>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if(Auth::check())
                    <li>
                        <a href="{{ url('dashboard') }}">Dashboard</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ url('bookings') }}">Bookings</a>
                    </li>
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="page-scroll" href="#about">About</a>
                </li>
                <li style="display: none;">
                    <a class="page-scroll" href="#services">Services</a>
                </li>
                <li>
                    <a class="page-scroll" href="#service-packages">Service Packages</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact">Contact</a>
                </li>
                @if(Auth::guest())
                    <li>
                        <a class="page-scroll" href="{{ url('auth/login') }}">Login</a>
                    </li>
                @else
                    <li>
                        <a class="page-scroll" href="{{ url('auth/logout') }}">Logout</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>