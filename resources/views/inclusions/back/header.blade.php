<nav class="navbar navbar-red">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">Super Angels Balloons</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="{{ Active::pattern('dashboard') }}">
                    <a href="{{ url('dashboard') }}">
                        Dashboard
                    </a>
                </li>
                <li class="{{ Active::pattern(['bookings','bookings/*']) }}">
                    <a href="{{ url('bookings') }}">
                        Bookings
                    </a>
                </li>
                <li class="{{ Active::pattern(['packages','packages/*']) }}">
                    <a href="{{ url('packages') }}">
                        Service Packages
                    </a>
                </li>
                @if(Auth::user()->isAdmin())
                    @include('modules.admin')
                @else
                    @include('modules.customer')
                @endif
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        {{ ucwords(Auth::user()->profile->first_name . ' ' . Auth::user()->profile->last_name) }}
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('profile') }}">Profile</a></li>
                        <li><a href="{{ url('auth/logout') }}">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>