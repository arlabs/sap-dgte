<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <em>Powered by:</em> <b><a href="http://almsaeedstudio.com">Almsaeed Studio</a></b>
    </div>
    Copyright &copy; 2014-2015 <strong><a href="{{ url('/') }}">Super Angels Balloons</a></strong>. All rights reserved.
</footer>