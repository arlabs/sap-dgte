<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/images{{ Auth::user()->profile->image ? '/costumers/' . Auth::user()->profile->image : '/defaults/avatar.png' }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ ucwords(Auth::user()->profile->first_name . ' ' . Auth::user()->profile->last_name) }}</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ Active::pattern('dashboard') }}">
                <a href="{{ url('dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ Active::pattern(['bookings','bookings/*']) }}">
                <a href="{{ url('bookings') }}">
                    <i class="fa fa-file-text"></i> <span>Bookings</span>
                </a>
            </li>
            <li class="{{ Active::pattern(['packages','packages/*']) }}">
                <a href="{{ url('packages') }}">
                    <i class="fa fa-th"></i> <span>Service Packages</span>
                </a>
            </li>
            @if(Auth::user()->isAdmin())
                @include('modules.admin')
            @else
                @include('modules.customer')
            @endif
        </ul>
    </section>
</aside>