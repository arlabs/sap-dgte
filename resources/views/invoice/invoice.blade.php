<section class="invoice">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Super Angels Balloons
                <small class="pull-right"><b>Ref #:</b> {{ $reference_number }}</small>
            </h2>
        </div>
    </div>

    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            From
            <address>
                <strong>Super Angels Balloons</strong><br>
                Cervantes Street,<br>
                Dumaguete City, 6200<br>
                Phone: 226-1992 | 09239420548
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            To
            <address>
                <strong>{{ ucwords($costumer->profile->first_name . ' ' . $costumer->profile->last_name) }}</strong><br>
                {{ ucwords($costumer->profile->barangay.', ') }}
                {{ ucwords($costumer->profile->location->name) }}<br>
                Phone: {{ $costumer->profile->mobile }}<br/>
                Email: {{ $costumer->email }}
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            <b>Event:</b><br/>
            <i class="fa fa-map-marker"></i> &nbsp; {{ $barangay ? ucwords($barangay).', ' : '' }}{{ ucwords($location->name) }}<br/>
            <i class="fa fa-calendar"></i> {{ $event_date }}<br/>
            <span class="hidden"><b>Payment Due:</b> -<br/></span>
            <input type="hidden" name="event_date" value="{{ Session::has('event_date') ? Session::get('event_date') : null }}"/>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th style="width: 5%;">Qty</th>
                    <th>Package(s)</th>
                    <th>Description</th>
                    <th class="text-center" style="width: 10%;">Subtotal</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($packages as $package)
                        <tr style="border-bottom: 1px solid #DDDDDD;">
                            <td>1</td>
                            <td>{{ ucwords($package->name) }}</td>
                            <td style="text-align: left;">
                                @if($package->category === 'main')
                                    <input type="hidden" name="main_package_id" value="{{ $package->id }}" placeholder="main"/>
                                @else
                                    <input type="hidden" name="additional_package_id" value="{{ $package->id }}" placeholder="additional"/>
                                @endif
                                <ul>
                                    <?php $addPackSubtotal = 0; ?>
                                    @foreach($package->descriptions as $desc)
                                        <li>
                                            {{ $desc->content }}
                                            {{ $package->category == 'additional' ? '- &#8369;'.number_format($desc->price,2) : '' }}

                                            @if($package->category === 'main')
                                                <input type="hidden" name="main_description_ids[]" value="{{ $desc->id }}" placeholder="main"/>
                                            @else
                                                <input type="hidden" name="additional_description_ids[]" value="{{ $desc->id }}" placeholder="additional"/>
                                            @endif

                                            <?php $addPackSubtotal += $desc->price; ?>
                                        </li>
                                    @endforeach
                                </ul>
                            </td>
                            <td style="text-align: right;">
                                @if($package->category === 'main')
                                    {{ $isMainPackageCustomized ? '-' : '&#8369;' . number_format($package->price,2) }}
                                @else
                                    {{ $isMainPackageCustomized ? '-' : '&#8369;' . number_format($addPackSubtotal,2) }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="alert well well-sm">
                <h4><i class="icon fa fa-info-circle"></i>Quick Note:</h4>
                ***Out of town charges apply. Client may add prizes for the games. The show will start exactly on time booked by the client.***
            </div>
        </div>
        <div class="col-xs-6">

            <span class="text-danger">*Amounts may not appear if package is customized. Booking needs to be reviewed by Admin.</span>

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Subtotal:</th>
                        <td style="text-align: right;">
                            @if($for == 'review')
                                @if($isMainPackageCustomized)
                                    -
                                @else
                                    &#8369;{{ number_format($subtotal,2) }}
                                @endif
                            @else
                                @if($total == 0)
                                    -
                                @else
                                    &#8369;{{ number_format($total - $location->price,2) }}
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Out of Town Charge ({{ ucwords($location->name) . ' - ' . $location->km . 'km' }})</th>
                        <td style="text-align: right;">
                            &#8369;{{ number_format($location->price,2) }}
                            <input type="hidden" name="location_id" value="{{ $location->id }}">
                            <input type="hidden" name="barangay" value="{{ $barangay }}">
                        </td>
                    </tr>
                    @if($for == 'viewing')
                        <tr>
                            <th colspan="2">Costumer Payment:</th>
                        </tr>
                        @foreach($booking->payments as $payment)
                            <tr>
                                <td style="text-indent: 25px; text-align: left;">{{ $payment->created_at->format('d-M-Y h:i A') }}</td>
                                <td style="text-align: right;">(&#8369;{{ number_format($payment->amount,2) }})</td>
                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <th>Total:</th>
                        <td style="text-align: right; font-weight: bold;">
                            @if($for == 'review')
                                @if($isMainPackageCustomized)
                                    -
                                @else
                                    &#8369;{{ number_format($total,2) }}
                                @endif
                            @else
                                @if($total == 0)
                                    -
                                @else
                                    &#8369;{{ number_format($booking->balance(),2) }}
                                @endif
                            @endif
                            <input type="hidden" name="total" value="{{ $total }}"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    @if($for == 'review')
    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <hr/>
            <a href="invoice-print.html" target="_blank" class="btn btn-default hidden"><i class="fa fa-print"></i> Print</a>
            <button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-paper-plane"></i> Submit For Review</button>
            <button class="btn btn-primary pull-right hidden" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
        </div>
    </div>
    @endif
</section>
<div class="clearfix"></div>