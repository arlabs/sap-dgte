<p>Hello, {{ $name }}!</p>

<p>
    Thank you for booking a package with us. Your booking reference number is displayed below:
</p>

<h2>
    {{ $booking['reference_number'] }}
</h2>

<p>
    Please let our staff review your booking details and we'll get back to you very soon.
</p>

<p>
    Regards,<br/>
    Super Angels Balloons
</p>