<p>Hello, {{ $name }}!</p>

<p>
    You have just changed your password. Use the new password the next time you log in.
</p>

<p>
    Regards,<br/>
    Super Angels Balloons
</p>