<p>Hello, {{ $name }}!</p>

<p>
    We have received your payment and deducted your balance based on the amount you paid.
    Click the reference number below to view your invoice.
</p>

<p>
    @foreach($latest_payment as $latest)
        <strong>Amount:</strong> &#8369;{{ number_format($latest['amount'],2) }}<br/>
        <strong>Received:</strong> {{ date('d-M-Y h:i A', strtotime($latest['created_at'])) }}<br/>
    @endforeach
    <strong>Ref #:</strong> <a href="{{ url('bookings/invoice/' . $booking['reference_number']) }}">{{ $booking['reference_number'] }}</a>
</p>

<p>
    Regards,<br/>
    Super Angels Balloons
</p>