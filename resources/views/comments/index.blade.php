@extends('back')

@section('content')

    <section class="content-header">
        <h1>{{ ucwords($title) }}</h1>
    </section>

    <section class="content">

        <div class="box">

            <div class="box-body">

                {!! Form::open(['url' => 'comments/viewable', 'id' => 'form-comments-viewable', 'class' => 'on-submit-disable']) !!}
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>User</th>
                        <th>Content</th>
                        <th style="width: 10%;">Viewable</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($comments as $comment)
                            <tr>
                                <td>{{ $comment->created_at->format('d-M-Y') }}</td>
                                <td>{{ ucwords($comment->user->profile->first_name.' '.$comment->user->profile->last_name) }}</td>
                                <td style="text-align: left;">{{ $comment->content }}</td>
                                <td>
                                    <div class="checkbox icheck">
                                        <label>
                                            <input
                                                type="checkbox"
                                                name="id[]"
                                                class="viewable"
                                                value="{{ $comment->id }}"
                                                {{ $comment->viewable ? 'checked' : '' }}
                                            >
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                        <td>
                            <button
                                type="submit"
                                class="btn btn-danger btn-block btn-flat btn-sm"
                                data-toggle="tooltip"
                                data-placement="left"
                                title="Make comments (checked) viewable in comments section"
                            >Viewable</button>
                        </td>
                    </tr>
                    </tfoot>
                </table>
                {!! Form::close() !!}

                {!! $comments->render() !!}

                @unless($comments->count())
                    <p class="help-block">No comments available</p>
                @endunless

            </div>

        </div>

    </section>

@endsection