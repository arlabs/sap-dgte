@extends('front')

@section('content')

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>Super Angels Balloons</h1>
                <hr>
                <p>We have service packages that are perfect for your events like birthday parties.</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
                @if(Auth::guest())
                    <a href="{{ url('auth/register') }}" class="btn btn-default btn-xl">Register Now!</a>
                @endif
            </div>
        </div>
    </header>

    @include('pages._about')

    @include('pages._services')

    @include('pages._service-packages')

    @include('pages._comments')

    @include('pages._contact')

@endsection