<section class="" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <h2 class="section-heading">Who We Are</h2>
                <hr class="primary">
                <p>
                    Super Angels Balloons is one of the bestselling and budget friendly for any kind of balloons, not only balloons but also anything that you want to make your party happy. It offers many services that helps in your party needs like cakes, magician, mascot, face painting, food carts, canopy, tables and chairs, party host, catering equipment, and balloons decoration and set-up.
                </p>
                <p>
                    Super Angels Balloons was established on 1994. It is conveniently located at Cervantes St. Dumaguete City.
                </p>
            </div>
        </div>
    </div>
</section>