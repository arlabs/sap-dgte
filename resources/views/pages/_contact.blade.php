<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1">
                <img src="/images/map.jpg" class="img-responsive" />
            </div>
            <div class="col-lg-5 text-center">
                <h2 class="section-heading" style="margin-top: 100px;">Let's Get In Touch!</h2>
                <hr class="primary">
                <p>Ready to book a service with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>

                <i class="fa fa-phone fa-3x"></i>
                <p>226-1992 | 09239420548</p>
            </div>
        </div>
    </div>
</section>