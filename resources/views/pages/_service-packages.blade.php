<section class="no-padding" id="service-packages">
    <div class="container-fluid">
        <div class="row no-gutter">
            @foreach($packages as $package)
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img
                            src="/images{{ $package->image ? '/packages/' . $package->image : '/defaults/packages.png' }}"
                            class="img-responsive"
                            alt="..."
                            style="width: 100%;"
                        />
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    {{ ucwords($package->category) }} Package
                                </div>
                                <div class="project-name">
                                    {{ ucwords($package->name) }}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach

            @unless($packages->count())
                <p class="help-block text-center">No packages available</p>
            @endunless
        </div>
    </div>
</section>