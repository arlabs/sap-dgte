<aside class="bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading wow tada">Comments</h2>
                <hr class="primary">
            </div>
        </div>
    </div>
    <div class="container text-center">
        <div class="call-to-action">
            <div class="comment">
                @foreach($comments as $comment)

                    <div class="item">
                        <p class="message">"{{ $comment->content }}"</p>

                        <img src="/images{{ $comment->user->profile->image ? '/costumers/' . $comment->user->profile->image : '/defaults/avatar.png' }}"/>
                        <span>&nbsp;{{ ucwords($comment->user->profile->first_name.' '.$comment->user->profile->last_name) }}</span>
                    </div>

                @endforeach

                @unless($comments->count())
                    <p>Be the first one to comment!</p>
                @endunless
            </div>

            @if(Auth::check())
                @if(Auth::user()->isCostumer())
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            {!! Form::open(['url' => 'comments', 'id' => 'form-comment']) !!}

                                <div class="form-group">
                                    {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => 5, 'placeholder' => 'Your comment here...']) !!}
                                </div>

                                <div class="form-group">
                                    <span class="message"></span>
                                    <button type="submit" class="btn btn-default pull-right"><i class="fa fa-comment"></i> Comment</button>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                @endif
            @else
                <a href="{{ url('auth/login') }}">Login</a> to drop a comment
            @endif
        </div>
    </div>
</aside>