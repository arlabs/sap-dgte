@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('locations') }}" class="btn btn-default btn-sm pull-right">Back to list</a>

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-body">

                {!! Form::open(['url' => 'locations/' . $location->id]) !!}
                    @include('locations._form')
                {!! Form::close() !!}

            </div>
        </div>

    </section>

@endsection