<div class="row">
    <div class="col-md-4">

        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label>Name:</label>
            <input type="text" class="form-control" name="name" value="{{ $location ? $location->name : null }}" autofocus>
            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{ $errors->has('km') ? 'has-error' : '' }}">
            <label>Distance:</label>
            <input type="number" class="form-control" name="km" value="{{ $location ? $location->km : null }}" placeholder="Distance from Dumaguete in km">
            {!! $errors->first('km', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
            <label>Price:</label>
            <input type="number" class="form-control" name="price" value="{{ $location ? $location->price : null }}">
            {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <button type="submit" class="btn btn-danger btn-sm pull-right">Submit</button>
            </div>
        </div>

    </div>
</div>