@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('locations/create') }}" class="btn btn-success btn-flat btn-sm pull-right on-click-disable">
            <i class="fa fa-plus"></i> Add Location
        </a>

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-body">

                @if($locations->count())
                    <table class="table table-striped table-hover table-datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Distance from<br/>Dumaguete (km)</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($locations as $location)
                                <tr>
                                    <td>{{ $location->id }}</td>
                                    <td>{{ ucwords($location->name) }}</td>
                                    <td>{{ $location->km }}km</td>
                                    <td>P{{ number_format($location->price,2) }}</td>
                                    <td>
                                        <a href="{{ url('locations/' . $location->id) }}" class="btn btn-default btn-sm">Edit</a>
                                        <a href="#" class="btn btn-danger btn-sm btn-generic-delete" data-id="{{ $location->id }}" data-url="locations"><i class="fa fa-times"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

                @unless($locations->count())
                    <p class="help-block">No users available</p>
                @endunless
            </div>
        </div>

    </section>

@endsection