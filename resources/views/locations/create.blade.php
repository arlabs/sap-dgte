@extends('back')

@section('content')

    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-body">

                {!! Form::open(['url' => 'locations']) !!}
                    @include('locations._form')
                {!! Form::close() !!}

            </div>
        </div>

    </section>

@endsection