@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('profile') }}" class="btn btn-default btn-flat btn-sm pull-right"><i class="fa fa-arrow-left"></i> Back</a>

        <h1>{{ ucwords($title) }}</h1>
    </section>

    <section class="content">

        <div class="box">

            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a href="#" class="thumbnail">
                          <img src="/images{{ Auth::user()->profile->image ? '/costumers/' . Auth::user()->profile->image : '/defaults/avatar.png' }}" alt="...">
                        </a>
                    </div>

                    <div id="profile" class="col-md-9 col-sm-12">

                        {!! Form::open(['url' => 'upload', 'files' => 'true']) !!}
                            <h3>Edit profile picture</h3>

                            <span class="btn btn-default btn-file">
                                <i class="fa fa-image"></i> Browse {!! Form::file('image') !!}
                            </span>

                            <span class="message"></span>

                            {!! Form::hidden('path', 'costumers') !!}
                            {!! Form::hidden('id', Auth::id()) !!}
                        {!! Form::close() !!}

                        {!! Form::open(['url' => 'profile/update', 'class' => 'on-submit-disable']) !!}
                            <hr/>
                            <h3>Edit your details below</h3>

                            <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="text" class="form-control" name="first_name" value="{{ ucwords(Auth::user()->profile->first_name) }}" placeholder="First Name">
                                <i class="glyphicon glyphicon-user form-control-feedback"></i>
                                {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="text" class="form-control" name="last_name" value="{{ ucwords(Auth::user()->profile->last_name) }}" placeholder="Last Name">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('mobile') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="number" class="form-control" name="mobile" value="{{ Auth::user()->profile->mobile }}" placeholder="Mobile">
                                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('barangay') ? 'has-error' : '' }}" style="width: 50%;">
                                {!! Form::text('barangay', Auth::user()->profile->barangay, ['class' => 'form-control']) !!}
                                <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                                {!! $errors->first('barangay', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('location_id') ? 'has-error' : '' }}" style="width: 50%;">
                                {!! Form::select('location_id', $locations, Auth::user()->profile->location_id, ['class' => 'form-control']) !!}
                                <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                                {!! $errors->first('location_id', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group" style="width: 50%;">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-check"></i> Done</button>
                                        </div>
                                </div>
                            </div>
                        {!! Form::close() !!}

                        {!! Form::open(['url' => 'profile/change_password', 'class' => 'on-submit-disable']) !!}
                            <hr/>
                            <h3>Change your password</h3>

                            <div class="form-group has-feedback {{ $errors->has('current_password') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="password" name="current_password" class="form-control" placeholder="Current password"/>
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                {!! $errors->first('current_password', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('new_password') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="password" name="new_password" class="form-control" placeholder="Password"/>
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                {!! $errors->first('new_password', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('new_password_confirmation') ? 'has-error' : '' }}" style="width: 50%;">
                                <input type="password" name="new_password_confirmation" class="form-control" placeholder="Retype password"/>
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                {!! $errors->first('new_password_confirmation', '<span class="help-block">:message</span>') !!}
                            </div>

                            <div class="form-group" style="width: 50%;">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-lock"></i> Change</button>
                                        <span class="help-block">Please take note of your new password.</span>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>

    </section>

@endsection