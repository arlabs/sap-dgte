@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('profile/edit') }}" class="btn btn-success btn-flat btn-sm pull-right"><i class="fa fa-pencil"></i> Edit</a>

        <h1>{{ ucwords($title) }}</h1>
    </section>

    <section class="content">

        <div class="box">

            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a href="#" class="thumbnail">
                          <img src="/images{{ Auth::user()->profile->image ? '/costumers/' . Auth::user()->profile->image : '/defaults/avatar.png' }}" alt="...">
                        </a>
                    </div>

                    <div id="profile" class="col-md-9 col-sm-12">

                        <div class="labels">
                            <h1>{{ ucwords(Auth::user()->profile->first_name . ' ' . Auth::user()->profile->last_name) }}</h1>
                            <h4><i class="fa fa-map-marker"></i> &nbsp;&nbsp; {{ ucwords((Auth::user()->profile->barangay ? Auth::user()->profile->barangay.', ' : '') . Auth::user()->profile->location->name) }}</h4>
                            <h5><i class="fa fa-envelope"></i> &nbsp; {{ Auth::user()->email }}</h5>
                            <h5><i class="fa fa-phone"></i> &nbsp;&nbsp; {{ Auth::user()->profile->mobile }}</h5>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </section>

@endsection