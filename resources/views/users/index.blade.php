@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('users/create') }}" class="btn btn-success btn-flat btn-sm pull-right on-click-disable">
            <i class="fa fa-plus"></i> Add New User
        </a>

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-body">

                @if($users->count())
                    <table class="table table-striped table-hover table-datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Mobile</th>
                            <th>Date Joined</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>
                                        <a href="{{ url('users/'.$user->slug.'/edit') }}">
                                            {{ ucwords($user->profile->first_name.' '.$user->profile->last_name) }}
                                        </a>
                                    </td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        {{ ucwords(($user->profile->barangay ? $user->profile->barangay.', ' : '') . $user->profile->location->name) }}
                                    </td>
                                    <td>
                                        {{ $user->profile->mobile }}
                                    </td>
                                    <td>{{ $user->created_at->format('d-M-Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

                @unless($users->count())
                    <p class="help-block">No users available</p>
                @endunless
            </div>
        </div>

    </section>

@endsection