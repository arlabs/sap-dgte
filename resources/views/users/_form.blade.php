<div class="row">
    <div class="col-md-4">
        @if($create)
        <div class="form-group has-feedback {{ $errors->has('role_id') ? 'has-error' : '' }}">
            {!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            {!! $errors->first('role_id', '<span class="help-block">:message</span>') !!}
        </div>
        @endif

        <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}">
            <input type="text" class="form-control" name="first_name" value="{{ $user ? $user->profile->first_name : null }}" placeholder="First Name" autofocus>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}">
            <input type="text" class="form-control" name="last_name" value="{{ $user ? $user->profile->last_name : null }}" placeholder="Last Name">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group has-feedback {{ $errors->has('mobile') ? 'has-error' : '' }}">
            <input type="number" class="form-control" name="mobile" value="{{ $user ? $user->profile->mobile : null }}" placeholder="Mobile">
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group has-feedback">
            <input type="text" class="form-control" name="barangay" value="{{ $user ? $user->profile->barangay : null }}" placeholder="Barangay">
            <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback {{ $errors->has('location_id') ? 'has-error' : '' }}">
            {!! Form::select('location_id', $locations, $user ? $user->profile->location->id : null, ['class' => 'form-control']) !!}
            <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
            {!! $errors->first('location_id', '<span class="help-block">:message</span>') !!}
        </div>

        @if($create)
        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
            <input type="email" class="form-control" name="email" value="{{ $user ? $user->email : null }}" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
            <input type="password" name="password" class="form-control" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
            <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password"/>
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
        </div>
        @endif

        <div class="row">
            <div class="col-xs-8"></div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-success btn-sm btn-flat pull-right"><i class="fa fa-plus"></i> Submit</button>
            </div>
        </div>
    </div>
</div>