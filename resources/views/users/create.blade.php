@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('users/create') }}" class="btn btn-success btn-flat btn-sm pull-right on-click-disable">
            <i class="fa fa-plus"></i> Add New User
        </a>

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-body">

                {!! Form::open(['url' => 'users', 'class' => 'on-submit-disable']) !!}

                    @include('users._form', ['create' => true])

                {!! Form::close() !!}

            </div>
        </div>

    </section>

@endsection