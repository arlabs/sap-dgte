@extends('back')

@section('content')

    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-body">

                {!! Form::open(['url' => 'users/'.$user->slug, 'class' => 'on-submit-disable']) !!}

                    @include('users._form', ['create' => false])

                {!! Form::close() !!}

            </div>
        </div>

    </section>

@endsection