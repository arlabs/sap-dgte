{!! Form::open(['url' => $request->url . '/' . $request->id, 'method' => 'DELETE']) !!}
<div class="modal-body text-center">
    Are you sure you want to delete this?
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
</div>
{!! Form::close() !!}

<script>
    $(function() {
        $('button[type=submit]').on('click', function(e){
            $(this).addClass('disabled');
        });
    });
</script>