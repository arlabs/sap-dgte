@extends('back')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Dashboard</h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $unpaidCounter }}</h3>
                        <a href="{{ url('bookings') }}">Unpaid Bookings</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $pendingCounter }}</h3>
                        <a href="{{ url('bookings') }}">Pending Bookings</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $activeCounter }}</h3>
                        <a href="{{ url('bookings') }}">Active Bookings</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-white">
                    <div class="inner">
                        <h3>{{ $packagesCounter }}</h3>
                        <a href="{{ url('packages') }}">Service Packages</a>
                    </div>
                </div>
            </div>
        </div>

        @if(Auth::user()->isAdmin())
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Registered Users</h3>
                </div>
                <div class="box-body">

                    <table class="table table-striped table-hover table-datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th>Date<br/>Registered</th>
                            <th style="width: 10%;">Bookings</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ ucwords($user->profile->first_name.' '.$user->profile->last_name) }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->profile->mobile }}</td>
                                    <td>
                                        {{ ucwords(($user->profile->barangay ? $user->profile->barangay.', ' : '') . $user->profile->location->name) }}
                                    </td>
                                    <td>{{ $user->created_at->format('d-M-Y') }}</td>
                                    <td>{{ $user->bookings->count() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @unless($users->count())
                        <p class="help-block">No registered users</p>
                    @endunless

                </div>
            </div>
        @endif

    </section>

@endsection