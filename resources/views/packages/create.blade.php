@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('packages') }}" class="btn btn-default btn-flat btn-sm pull-right on-click-disable">
            <i class="fa fa-arrow-left"></i> Back to list
        </a>

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        {!! Form::open(['url' => 'packages', 'class' => 'on-submit-disable form-package']) !!}

            @include('packages._form', ['type' => 'create'])

        {!! Form::close() !!}

    </section>

@endsection