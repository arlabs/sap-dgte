@extends('back')

@section('content')

    <section class="content-header">
        <div class="pull-right">
            <a href="{{ url('packages') }}" class="btn btn-default btn-flat btn-sm on-click-disable">
                <i class="fa fa-arrow-left"></i> Back to list
            </a>
        </div>

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        {!! Form::open(['url' => 'packages/' . $package->id, 'class' => 'on-submit-disable form-package']) !!}

            @include('packages._form', ['type' => 'edit'])

        {!! Form::close() !!}

    </section>

@endsection