@extends('back')

@section('content')

    <section class="content-header">
        @if(Auth::user()->isAdmin())
            <a href="{{ url('packages/create') }}" class="btn btn-success btn-flat btn-sm pull-right on-click-disable">
                <i class="fa fa-plus"></i> Add Package
            </a>
        @endif

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        @foreach(array_chunk($Packages->all(), 4) as $packages)
            <div class="row">
                @foreach($packages as $package)
                    <div class="col-md-3 col-sm-6">
                        <div class="thumbnail">
                            <img src="/images{{ $package->image ? '/packages/' . $package->image : '/defaults/packages.png' }}" alt="...">
                            <div class="caption">
                                <h4 class="text-center">{{ ucwords($package->name) }}</h4>

                                <div class="list-group">
                                    <?php $count = 1; ?>
                                    @foreach($package->descriptions as $desc)
                                        @if($count <= 2)
                                            <span class="list-group-item">
                                                @if($package->category == 'additional')
                                                    <span class="badge">&#8369;{{ number_format($desc->price,2) }}</span>
                                                @endif

                                                {{ str_limit($desc->content,15) }}
                                            </span>
                                        @endif
                                        <?php $count++; ?>
                                    @endforeach
                                </div>
                                <a href="{{ url('packages/' . $package->id) }}" class="btn btn-default btn-flat btn-sm on-click-disable">
                                    <i class="fa fa-search"></i> View
                                </a>

                                @if(Auth::user()->isAdmin())
                                    <a href="#"
                                        class="btn btn-danger btn-flat btn-sm pull-right btn-generic-delete"
                                        data-id="{{ $package->id }}"
                                        data-url="packages"
                                    >
                                        <i class="fa fa-times"></i> Delete
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach

        @unless($Packages->count())
            <p class="help-block">No packages available</p>
        @endunless

        {!! $Packages->render() !!}

    </section>

@endsection