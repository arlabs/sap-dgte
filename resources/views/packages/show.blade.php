@extends('back')

@section('content')

    <section class="content-header">
        <div class="pull-right">
            <a href="{{ url('packages') }}" class="btn btn-default btn-flat btn-sm on-click-disable">
                <i class="fa fa-arrow-left"></i> Back to list
            </a>
            @if(Auth::user()->isAdmin())
                <a href="{{ url('packages/' . $package->id . '/edit') }}" class="btn btn-success btn-flat btn-sm on-click-disable">
                    <i class="fa fa-pencil"></i> Edit
                </a>
            @endif
        </div>

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="row package-details">
            <div class="col-md-4 col-md-offset-4">
                <div class="thumbnail" style="position: relative;">
                    <img src="/images{{ $package->image ? '/packages/' . $package->image : '/defaults/packages.png' }}" alt="...">

                    @if(Auth::user()->isAdmin())
                        {!! Form::open(['url' => 'upload', 'files' => 'true', 'id' => 'form-package-upload']) !!}
                            <span class="btn btn-default btn-flat btn-file">
                                <i class="fa fa-image"></i> Change {!! Form::file('image') !!}
                            </span>
                            {!! Form::hidden('path', 'packages') !!}
                            {!! Form::hidden('id', $package->id) !!}
                        {!! Form::close() !!}
                        <p class="text-center message"></p>
                    @endif

                    <div class="caption">
                        <h4 class="text-center">{{ ucwords($package->name) }}</h4>
                        <span class="help-block text-center">
                            {{ ucwords($package->category . ' Package') }}

                            @if($package->category == 'main')
                                <br/><span class="badge">&#8369;{{ number_format($package->price,2) }}</span>
                            @endif
                        </span>

                        <ul class="list-group">
                            @foreach($package->descriptions as $desc)
                                <li class="list-group-item">
                                    @if($package->category == 'additional')
                                        <span class="badge">&#8369;{{ number_format($desc->price,2) }}</span>
                                    @endif

                                    {{ $desc->content }}
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection