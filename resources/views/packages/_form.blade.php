@if(Auth::user()->isAdmin())
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Package Details</h3>
                </div>
                <div class="box-body">
                    <div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
                        {!! Form::label('category', 'Category:') !!}
                        {!! Form::select('category', ['main' => 'Main Package','additional' => 'Additional Package'], $package ? $package->category : null,['class' => 'form-control']) !!}
                        {!! $errors->first('category', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name', $package ? ucwords($package->name) : null,['class' => 'form-control', 'placeholder' => '']) !!}
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                        {!! Form::label('price', 'Price:') !!}
                        {!! Form::number('price', $package ? $package->price : 0,['class' => 'form-control', 'id' => 'package_price']) !!}
                        {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Set a description</h3>

                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">
                    <p class="text-danger text-center error-msg"></p>

                    @if($type == 'create')
                        <div id="source-clone" class="form-group row">
                            <div class="col-xs-6">
                                <input type="text" name="description[]" class="form-control input-sm required" placeholder="Description"/>
                            </div>
                            <div class="col-xs-4">
                                <input type="number" name="desc_price[]" value="0" class="form-control input-sm required desc_price" placeholder="Price" style="display: none;">
                            </div>
                            <div class="col-xs-2">
                                <button type="button" class="btn btn-success btn-sm btn-block btn-add-description" title="Add more field"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    @else
                        @foreach($package->descriptions as $key => $description)
                            @if($key == 0)
                                <div id="source-clone" class="form-group row">
                                    <div class="col-xs-6">
                                        <input type="text" name="description[]" class="form-control input-sm required" value="{{ $description->content }}" placeholder="Description"/>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="number" name="desc_price[]" class="form-control input-sm required desc_price" value="{{ $description->price }}" placeholder="Price">
                                    </div>
                                    <div class="col-xs-2">
                                        <button type="button" class="btn btn-success btn-sm btn-block btn-add-description" title="Add more field"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            @else
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <input type="text" name="description[]" class="form-control input-sm required" value="{{ $description->content }}" placeholder="Description"/>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="number" name="desc_price[]" class="form-control input-sm required desc_price" value="{{ $description->price }}" placeholder="Price">
                                    </div>
                                    <div class="col-xs-2">
                                        <button type="button" class="btn btn-danger btn-sm btn-block btn-remove-description" title="Remove field"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-success btn-flat pull-right"><i class="fa fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
@endif