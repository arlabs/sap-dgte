@extends('auth.template')

@section('content')

    <div class="login-logo">
        <a href="{{ url('auth/login') }}"><b>Register</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Register a new membership</p>

        <form role="form" method="POST" action="{{ url('/auth/register') }}" class="on-submit-disable">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}">
                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" autofocus>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}">
                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group has-feedback {{ $errors->has('mobile') ? 'has-error' : '' }}">
                <input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile">
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                {!! $errors->first('mobile', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="barangay" value="{{ old('barangay') }}" placeholder="Barangay">
                <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback {{ $errors->has('location_id') ? 'has-error' : '' }}">
                {!! Form::select('location_id', $locations, null, ['class' => 'form-control']) !!}
                <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
                {!! $errors->first('location_id', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                <input type="password" name="password" class="form-control" placeholder="Password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password"/>
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck hidden">
                        <label>
                            <input type="checkbox"> I agree to the <a href="#">terms</a>
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-danger btn-block btn-flat"><i class="fa fa-pencil"></i> Register</button>
                </div>
            </div>
        </form>

        <div class="social-auth-links text-center">
            <p>- Already got an account? -</p>
            <a href="{{ url('auth/login') }}" class="btn btn-success btn-block btn-flat">Sign in to start your session</a>
        </div>

    </div>

@endsection
