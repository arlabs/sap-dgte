@extends('auth.template')

@section('content')

    <div class="login-logo">
        <a href="{{ url('auth/login') }}"><b>Sign in</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form role="form" method="POST" action="{{ url('/auth/login') }}" class="on-submit-disable">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email" autofocus/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <a class="text-center" href="{{ url('/password/email') }}">Forgot your password?</a>

                    <div class="checkbox icheck hidden">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-danger btn-block btn-flat"><i class="fa fa-sign-in"></i> Login</button>
                </div>
            </div>
        </form>

        <div class="social-auth-links text-center">
            <p>- Got no account? -</p>
            <a href="{{ url('auth/register') }}" class="btn btn-success btn-block btn-flat">Register a new membership</a>
        </div>

    </div>

@endsection
