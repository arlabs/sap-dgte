@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('bookings') }}"
            class="btn btn-default btn-flat pull-right on-click-disable"
            style="margin-right: 10px;"
        >
            <i class="fa fa-arrow-left"></i> Back to Booking list
        </a>

        <h1 style="margin-left: 10px;">{{ $title }}</h1>
    </section>

    {!! Form::open(['url' => 'bookings/send_review', 'class' => 'on-submit-disable']) !!}
        @include('invoice.invoice', ['for' => 'review'])
    {!! Form::close() !!}

@endsection