@extends('back')

@section('content')

    <section class="content-header">
        <a href="{{ url('bookings/invoice/' . $booking->reference_number) }}"
            class="btn btn-warning btn-flat pull-right on-click-disable"
            data-toggle="tooltip"
            data-placement="left"
            title="Proceed to invoice">
            <i class="fa fa-credit-card"></i> Invoice
        </a>

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Booking Details</h3>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    {!! Form::open(['url' => 'bookings/confirm/'.$booking->reference_number, 'class' => 'on-submit-disable']) !!}
                        <div class="box-body">

                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Costumer</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Location</th>
                                    <th style="width: 15%;">Booking Status</th>
                                    <th style="width: 15%;">Amount</th>
                                    <th style="width: 15%;">Payment Status</th>
                                </tr>
                                </thead>
                                <tbody style="border-bottom: 1px solid #e9e9e9;">
                                <tr>
                                    <td>
                                        {{ ucwords($booking->user->profile->first_name.' '.$booking->user->profile->last_name) }}
                                    </td>
                                    <td>
                                        {{ $booking->start_date->format('d-M-Y') }}
                                    </td>
                                    <td>
                                        {{ $booking->start_date->format('h:i A') }} - {{ $booking->end_date->format('h:i A') }}
                                    </td>
                                    <td>
                                        {{ $booking->barangay ? ucwords($booking->barangay.', ') : '' }}
                                        {{ ucwords($booking->location->name) }}
                                    </td>
                                    <td>
                                        <label class="label label-{{ $booking->status_level('booking') }}">
                                            {{ ucwords($booking->status) }}
                                        </label>
                                    </td>
                                    <td style="text-align: right;">
                                        <abbr
                                            data-toggle="tooltip"
                                            title="
                                                @if($booking->price > 0)
                                                    {{ number_format(($booking->price - $booking->location->price),2) }}
                                                @else
                                                    0
                                                @endif
                                                + {{ number_format($booking->location->price,2) }} (Out of Town Charge)
                                            "
                                        >
                                            @if($booking->price == 0)
                                                &#8369;{{ number_format($booking->location->price,2) }}
                                            @else
                                                &#8369;{{ number_format($booking->price,2) }}
                                            @endif
                                        </abbr>
                                    </td>
                                    <td>
                                        <span class="label label-{{ $booking->status_level('payment') }}">
                                            {{ ucwords($booking->payment_status()) }}
                                        </span>
                                    </td>
                                </tr>
                                <tr style="background-color: #f5f5f5;">
                                    <td colspan="4">&nbsp;</td>
                                    <td>
                                        {!! Form::select('status',['pending' => 'Pending', 'active' => 'Active', 'cancelled' => 'Cancelled'], $booking->status, ['class' => 'form-control input-sm', 'required']) !!}
                                    </td>
                                    <td>
                                        <div class="form-group {{ $booking->price == 0 ? 'has-error' : '' }}">
                                            <input
                                                type="number"
                                                name="amount"
                                                class="form-control input-sm"
                                                value="{{ ($booking->price > 0) ? $booking->price - $booking->location->price : $booking->price }}"
                                                placeholder="Amount"
                                                data-toggle="tooltip"
                                                title="+ {{ number_format($booking->location->price,2) }} (Out of Town Charge)"
                                                required
                                            />
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="push-sm"></div>

                            <div class="row">
                                <div class="col-xs-6">&nbsp;</div>
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-success btn-sm btn-flat pull-right"><i class="fa fa-check"></i> Update</button>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Payment Details</h3>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    {!! Form::open(['url' => 'bookings/set_payment/' . $booking->reference_number, 'class' => 'on-submit-disable']) !!}
                        <div class="box-body">

                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Date Paid</th>
                                    <th>Amount Paid</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($booking->payments as $payment)
                                        <tr>
                                            <td>{{ $payment->created_at->format('d-M-Y h:i A') }}</td>
                                            <td style="text-align: right;">&#8369;{{ number_format($payment->amount,2) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            @unless($booking->payments->count())
                                <p class="help-block">No payments made</p>
                            @endunless

                            <table class="table" style="background-color: #f5f5f5;">
                                <tbody>
                                @if($booking->payment_status() == 'unpaid')
                                    <tr>
                                        <td style="text-align: right; font-weight: bold;">Enter Amount:</td>
                                        <td>
                                            <input
                                                type="number"
                                                name="amount"
                                                class="form-control input-sm"
                                                placeholder="Costumer payment"
                                                value="{{ Session::get('amount') }}"
                                            />
                                        </td>
                                    </tr>
                                @endif
                                    <tr>
                                        <td style="text-align: right; font-weight: bold;">Balance:</td>
                                        <td style="text-align: right;">&#8369;{{ number_format($booking->balance(),2) }}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="push-sm"></div>

                            <div class="row">
                                <div class="col-xs-6">&nbsp;</div>
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-success btn-sm btn-flat pull-right"><i class="fa fa-check"></i> Set Payment</button>
                                </div>
                            </div>

                        </div>
                    {!! Form::close() !!}
                 </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Booked Package(s)</h3>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">

                        <ul>
                            @foreach($booked_packages as $package)
                                <li>
                                    <b>{{ ucwords($package->name) }}</b>
                                    <ul>
                                        @foreach($package->descriptions as $desc)
                                            <li>{{ $desc->content }}</li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Package(s)</h3>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">

                        <ul>
                            @foreach($packages as $package)
                                <li>
                                    <span>
                                        <b>{{ ucwords($package->name) }}</b>
                                        @if($package->category === 'main')
                                            <label class="label label-primary">
                                                {{ '&#8369;'.number_format($package->price,2) }}
                                            </label>
                                        @endif
                                    </span>

                                    <ul>
                                        @foreach($package->descriptions as $desc)
                                            <li>
                                                {{ $desc->content }}
                                                @if($package->category === 'additional')
                                                    <label class="label label-primary">
                                                        {{ '&#8369;'.number_format($desc->price,2) }}
                                                    </label>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection