@extends('back')

@section('content')

    <section class="content-header">
        @if(Auth::user()->isCostumer())
            <a href="{{ url('bookings/prepare') }}" class="btn btn-success btn-flat btn-sm pull-right on-click-disable">
                <i class="fa fa-plus"></i> Book a Service
            </a>
        @endif

        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-body">

                @if($bookings->count())
                    <table class="table table-striped table-hover table-datatable">
                        <thead>
                        <tr>
                            <th>Ref. #</th>
                            <th>Package(s)</th>
                            <th>Customer</th>
                            <th>Event Date</th>
                            <th>Location</th>
                            <th>Price</th>
                            <th>Booking<br/>Status</th>
                            <th>Payment<br/>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($bookings as $booking)
                                <tr>
                                    <td>
                                        @if(Auth::user()->isAdmin())
                                            <a href="{{ url('bookings/confirm/' . $booking->reference_number) }}">{{ $booking->reference_number }}</a>
                                        @else
                                            <a href="{{ url('bookings/invoice/' . $booking->reference_number) }}">{{ $booking->reference_number }}</a>
                                        @endif
                                    </td>
                                    <td style="text-align: left;">
                                        @foreach($booking->booked_packages as $index => $booked)
                                            {{ ($index + 1) . '. ' . ucwords($booked->package->name) }} <br/>
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ ucwords($booking->user->profile->first_name . ' ' . $booking->user->profile->last_name) }}
                                    </td>
                                    <td>{{ $booking->start_date->format('d-M-Y h:i A') }}</td>
                                    <td>
                                        {{ $booking->barangay ? ucwords($booking->barangay) . ', ' : '' }}
                                        {{ ucwords($booking->location->name) }}
                                    </td>
                                    <td style="text-align: right;">&#8369;{{ number_format($booking->price,2) }}</td>
                                    <td><label class="label label-{{ $booking->status_level('booking') }}">{{ ucwords($booking->status) }}</label></td>
                                    <td><label class="label label-{{ $booking->status_level('payment') }}">{{ ucwords($booking->payment_status()) }}</label></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif

                @unless($bookings->count())
                    <p class="help-block">No bookings available</p>
                @endunless
            </div>
        </div>

    </section>

@endsection