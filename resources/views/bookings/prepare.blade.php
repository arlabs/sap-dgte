@extends('back')

@section('content')

    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <section class="content">

        @include('bookings._form')

    </section>

@endsection