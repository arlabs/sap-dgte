<div class="row">
    <div class="col-md-6">

        <div class="box box-danger ">
            <div class="box-header with-border">
                <h3 class="box-title">Main Packages <small>- Choose one package</small></h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">

                @foreach($mainPackages as $package)
                    <div class="form-group">
                        <label class="package" data-package-id="{{ $package->id }}" data-package-name="{{ ucwords($package->name) }}">
                            <input
                                type="radio"
                                name="main"
                                class="package minimal-red"
                                data-package-id="{{ $package->id }}"
                                data-package-name="{{ ucwords($package->name) }}"
                                value="{{ $package->id }}"
                            />
                            {{ ucwords($package->name) }}
                            <label class="label label-danger">{{ $package->hours }} hour(s)</label>
                            <label class="label label-success">&#8369;{{ number_format($package->price,2) }}</label>
                        </label>

                        <input type="hidden" name="hours" value="{{ $package->hours }}">

                        <ul>
                            @foreach($package->descriptions as $description)
                                <li>{{ ucwords($description->content) }}</li>
                                <div class="form-group hidden source-descriptions">
                                    <label>
                                        <input type="checkbox" name="main_description_ids[]" class="minimal-red" value="{{ $description->id }}"/>
                                        {{ ucwords($description->content) }}
                                    </label>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                @endforeach

                @unless($mainPackages->count())
                    <p class="help-block">No packages available</p>
                @endunless

            </div>
        </div>

        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">{{ ucwords($additionalPackages->name) }} <small>- Choose additional (optional)</small></h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">

                <div class="form-group">
                    <span class="source-additional-package-id" data-package-id="{{ $additionalPackages->id }}"></span>
                    @foreach($additionalPackages->descriptions as $description)
                        <div class="form-group source-descriptions">
                            <label>
                                <input type="checkbox" name="additional_description_ids[]" class="minimal-red" value="{{ $description->id }}"/>
                                {{ ucwords($description->content) }}
                                <label class="label label-success">&#8369;{{ number_format($description->price,2) }}</label>
                            </label>
                        </div>
                    @endforeach
                </div>

                @unless($additionalPackages->count())
                    <p class="help-block">No packages available</p>
                @endunless

            </div>
            <div class="box-footer">
                <button type="button" class="btn btn-danger btn-sm btn-flat btn-add-selection">
                    <i class="fa fa-plus"></i> Add selected to My Booking Cart
                    </button>
            </div>
        </div>

    </div>

    <div class="col-md-6">
        <div class="box box-success">

            <div id="cart-cover">
                <h3 id="content" class="help-block">Choose a package</h3>
            </div>

            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> My Booking Cart</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            {!! Form::open(['url' => 'bookings/check_date', 'class' => 'form-check-date']) !!}
                <div class="box-body" style="padding: 10px 10px 0 10px;">
                    <div class="form-group">
                        <label for="event_date">Event Date:</label>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="input-group date" id="event_date">
                                    <input
                                        type="text"
                                        name="event_date"
                                        class="form-control input-sm"
                                        value="{{ Session::has('event_date') ? Session::get('event_date') : null }}"
                                    />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <input type="hidden" name="hours"/>
                                <button
                                    type="submit"
                                    class="btn btn-{{ Session::has('alert_level') ? Session::get('alert_level') : 'default' }} btn-sm btn-flat btn-block btn-check-date"
                                    >
                                    <i class="fa fa-check"></i> Check Availability
                                </button>
                            </div>
                        </div>
                        <span class="message"></span>
                    </div>
                </div>
            {!! Form::close() !!}

            {!! Form::open(['url' => 'bookings/review', 'class' => 'on-submit-disable form-continue']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="barangay">Event Location:</label>
                        <div class="row">
                            <div class="col-sm-7">
                                <input type="text" name="barangay" class="form-control input-sm" placeholder="Street, Barangay"/>
                            </div>
                            <div class="col-sm-5">
                                {!! Form::select('location_id', $locations, null, ['class' => 'form-control input-sm']) !!}
                            </div>
                        </div>
                    </div>

                    <hr/>

                    <label id="cloned-main-package-name"></label>
                    <input type="hidden" id="cloned-main-package-id" name="main_package_id"/>
                    <div id="cloned-main-descriptions"></div>

                    <label id="cloned-additional-package-name"></label>
                    <input type="hidden" id="cloned-additional-package-id" name="additional_package_id" value=""/>
                    <div id="cloned-additional-descriptions"></div>

                    <input
                        type="hidden"
                        name="event_date"
                        class="event-date-holder"
                        value="{{ Session::has('event_date') ? Session::get('event_date') : null }}"
                    />

                </div>
                <div class="box-footer">
                    <button type="button" class="btn btn-default btn-sm btn-flat btn-remove-unchecked"><i class="fa fa-times"></i> Remove unchecked</button>
                    <span class="error-msg text-danger"></span>
                    <button type="submit" class="btn btn-success btn-sm btn-flat pull-right btn-continue disabled" disabled>Continue &nbsp; <i class="fa fa-arrow-right"></i></button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(function() {
            $('#event_date').datetimepicker({
                useCurrent: false,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    next: "fa fa-chevron-right",
                    previous: "fa fa-chevron-left"
                }
            }).data("DateTimePicker").minDate(new Date);
        });
    </script>
@endsection