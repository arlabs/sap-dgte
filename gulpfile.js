var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix
        .styles([
            'bootstrap.min.css',
            'font-awesome.min.css',
            'datetimepicker/bootstrap-datetimepicker.min.css',
            'datatables/dataTables.bootstrap.css',
            'custom.css'
        ], './public/css/libs.css')
        .styles([
            'front/animate.min.css',
            'front/creative.css'
        ], './public/css/front.css')
        .styles([
            'back/AdminLTE.min.css',
            'back/skin-red.min.css',
            'back/iCheck/red.css'
        ], './public/css/back.css');

    mix
        .scripts([
            'jquery.js',
            'bootstrap.min.js',
            'datetimepicker/moment.js',
            'datetimepicker/bootstrap-datetimepicker.min.js',
            'datatables/jquery.dataTables.js',
            'datatables/dataTables.bootstrap.js',
            'custom.js'
        ], './public/js/libs.js')
        .scripts([
            'front/jquery.easing.min.js',
            'front/jquery.fittext.js',
            'front/wow.min.js',
            'front/creative.js'
        ], './public/js/front.js')
        .scripts([
            'back/icheck.min.js',
            'back/jquery.slimscroll.min.js',
            'back/fastclick.min.js',
            'back/app.min.js'
        ], './public/js/back.js');

    mix.version(['css/libs.css','css/front.css','css/back.css','js/libs.js','js/front.js','js/back.js']);

});
