<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateUsersRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'role_id' => 'required|numeric',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'mobile' => 'required|numeric',
            'location_id' => 'required|numeric',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
		];
	}

}
