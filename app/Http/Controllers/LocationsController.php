<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Location;
use Illuminate\Http\Request;

class LocationsController extends Controller {

	public function index()
    {
        $title = 'Locations';

        $locations = Location::all();

        return view('locations.index', compact('title','locations'));
    }

    public function create()
    {
        $title = 'Add Location';

        $location = [];

        return view('locations.create', compact('title', 'location'));
    }

    public function store(Request $request)
    {
        Location::create($request->all());

        return redirect('locations')->with([
            'alert-level' => 'success',
            'message' => 'Location has been successfully added.'
        ]);
    }

    public function show($id)
    {
        $location = Location::where('id','=',$id)->firstOrFail();

        $title = ucwords($location->name);

        return view('locations.show', compact('title', 'location'));
    }

    public function update(Request $request, $id)
    {
        $location = Location::where('id','=',$id)->firstOrFail();

        $location->name = $request->get('name');
        $location->km = $request->get('km');
        $location->price = $request->get('price');
        $location->save();

        return redirect('locations/' . $location->id)->with([
            'alert-level' => 'success',
            'message' => 'Location has been successfully updated.'
        ]);
    }

    public function destroy($id)
    {
        $location = Location::where('id','=',$id)->firstOrFail();

        $location->delete();

        return redirect('locations')->with([
            'alert-level' => 'success',
            'message' => 'Location has been successfully deleted.'
        ]);
    }

}
