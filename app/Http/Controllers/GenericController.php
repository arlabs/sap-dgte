<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Package;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class GenericController extends Controller {

    /**
     * Returns view for deleting an item.
     *
     * @param Request $request
     * @return mixed
     */
	public function delete(Request $request)
    {
        return view()->make('generic._delete', ['request' => $request])->render();
    }

    /**
     * Uploads image.
     *
     * @param Request $request
     * @return array
     */
    public function upload(Request $request)
    {
        // Valid extensions
        $validExtensions = array('jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG', 'bmp', 'BMP', 'gif', 'GIF');

        // Max file size (1mb)
        $maxSize = 1048576;

        // Upload directory
        $requestPath = $request->get('path');
        $path = public_path() . '/images/' . $requestPath . '/';

        // Set pixel
        if ($requestPath == 'costumers')
        {
            $pixelWidth = 400;
            $pixelHeight = 400;
        }
        elseif ($requestPath == 'packages')
        {
            $pixelWidth = 400;
            $pixelHeight = 250;
        }
        else
        {
            $pixelWidth = 0;
            $pixelHeight = 0;
        }

        // Get the file
        $file = $request->file('file');

        // Get file extension
        $extension = $file->getClientOriginalExtension();

        // Get file size
        $fileSize = $file->getClientSize();

        // Create new file name
        $newFileName = time().(rand(1,999)).'.'.$extension;

        // Create upload path if not exist
        if ( ! file_exists($path)) {
            mkdir($path, 0777);
        }

        // Check file size
        if ($fileSize > $maxSize)
        {
            return [
                'success' => false,
                'message' => 'File is too large.'
            ];
        }

        // Check if extension is valid
        if (in_array($extension, $validExtensions))
        {
            // Move uploaded file from temp to uploads directory
            if ($file->move($path, $newFileName))
            {
                $image = Image::make(sprintf($path.'/%s', $newFileName));

                $image->crop($pixelWidth, $pixelHeight);
                $image->resize($pixelWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image->save();

                // Save filename to Model
                if ($requestPath == 'costumers')
                {
                    $user = User::where('id','=',Auth::id())->first();
                    $oldImage = $user->profile->image;
                    $user->profile->image = $newFileName;
                    $user->profile->save();
                }
                elseif($requestPath == 'packages')
                {
                    $package = Package::where('id','=',$request->get('id'))->firstOrFail();
                    $oldImage = $package->image;
                    $package->image = $newFileName;
                    $package->save();
                }
                else
                {
                    return [
                        'success' => false,
                        'message' => 'Unknown error.'
                    ];
                }

                // Delete old image
                File::delete($path.$oldImage);

                return [
                    'success' => true,
                    'message' => 'File has been successfully uploaded.'
                ];
            }

            return [
                'success' => false,
                'message' => 'Unknown error.'
            ];
        }

        return [
            'success' => false,
            'message' => 'Invalid file extension.'
        ];
    }

}
