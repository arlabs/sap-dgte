<?php namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller {

    public function __construct()
    {
        $this->middleware('admin',['only' => ['index','viewable']]);

        $this->middleware('costumer',['only' => ['store']]);
    }

    /**
     * View comments.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $comments = Comment::with(['user'])->paginate(10);

        $title = 'Comments';

        return view('comments.index', compact('title','comments'));
    }

    /**
     * Make comments viewable.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function viewable(Request $request)
    {
        Comment::with(['user'])->where('viewable','=',1)->update(['viewable' => 0]);

        Comment::with(['user'])->whereIn('id',$request->get('id'))->update(['viewable' => 1]);

        return redirect()->back()->with([
            'alert-level' => 'success',
            'message' => 'Comments has been successfully set to viewable.'
        ]);
    }

    /**
     * Store comments.
     *
     * @param Request $request
     * @return array
     */
	public function store(Request $request)
    {
        Comment::create([
            'user_id' => Auth::id(),
            'content' => $request->get('content'),
            'viewable' => 0
        ]);

        return [
            'message' => 'Thank you for dropping a comment.'
        ];
    }

}
