<?php namespace App\Http\Controllers;

use App\Commands\ChangePassword;
use App\Http\Requests;
use App\Http\Requests\CreatePasswordRequest;
use App\Http\Requests\CreateProfileRequest;
use App\Location;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfilesController extends Controller {

    /**
     * Views profile details.
     *
     * @return \Illuminate\View\View
     */
	public function index()
    {
        $title = 'Profile';

        return view('profiles.index', compact('title'));
    }

    /**
     * Views edit profile details.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $locations = Location::lists('name','id');

        $title = 'Edit Profile';

        return view('profiles.edit', compact('title','locations'));
    }

    /**
     * Updates profile details.
     *
     * @param CreateProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CreateProfileRequest $request)
    {
        $user = User::with(['profile'])->where('id','=',Auth::id())->firstOrFail();

        User::doUpdate($user->slug, $request);

        return redirect('profile')->with([
            'alert-level' => 'success',
            'message' => 'Profile has been successfully updated.'
        ]);
    }

    /**
     * Changes user's password.
     *
     * @param CreatePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change_password(CreatePasswordRequest $request)
    {
        if ($this->passwordMatched($request))
        {
            $this->dispatch(new ChangePassword($request));
        }
        else
        {
            return redirect()->back()->with([
                'alert-level' => 'danger',
                'message' => 'The password you have entered did not match the current password.'
            ]);
        }

        return redirect('profile')->with([
            'alert-level' => 'success',
            'message' => 'Password has been successfully changed. Use your new password the next time you login.'
        ]);
    }

    /**
     * Checks if passwords matched.
     *
     * @param Request $request
     * @return bool
     */
    private function passwordMatched(Request $request)
    {
        $password = $request->get('current_password');

        $user = User::where('id','=',Auth::id())->first();

        if (Hash::check($password, $user->password))
        {
            return true;
        } else {
            return false;
        }
    }

}
