<?php namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests;
use App\Package;

class PagesController extends Controller {

    /**
     * Home page view.
     *
     * @return \Illuminate\View\View
     */
	public function index()
    {
        $packages = Package::with(['descriptions'])->take(6)->get();

        $comments = Comment::with(['user'])->where('viewable','=',1)->get();

        return view('pages.index', compact('comments','packages'));
    }

}
