<?php namespace App\Http\Controllers;

use App\Events\UserHasRegistered;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateProfileRequest;
use App\Http\Requests\CreateUsersRequest;
use App\Location;
use App\Profile;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UsersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $users = User::latest()
            ->with(['profile','profile.location','bookings','roles','comments'])
            ->where('id','!=',Auth::id())
            ->get();

        $title = 'Users';

        return view('users.index', compact('users','title'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $user = [];

        $locations = Location::lists('name','id');

        $roles = Role::lists('name','id');

		$title = 'Add New User';

        return view('users.create', compact('title','locations','roles','user'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateUsersRequest $request)
	{
        $user = User::create([
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        // Slug
        $user->slug = Str::slug($user->id.' '.$request->get('first_name').' '.$request->get('last_name'));
        $user->save();

        $profile = new Profile([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'mobile' => $request->get('mobile'),
            'barangay' => $request->get('barangay'),
            'location_id' => $request->get('location_id'),
        ]);
        $user->profile()->save($profile);

        $user->roles()->attach($request->get('role_id'));

        // Email welcome notification
//        event(new UserHasRegistered($user));

        return redirect('users')->with([
            'alert-level' => 'success',
            'message' => 'User has been successfully added.'
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($slug)
	{
		$user = User::where('slug','=',$slug)->firstOrFail();

        $roles = Role::lists('name','id');

        $locations = Location::lists('name','id');

        $title = ucwords($user->profile->first_name.' '.$user->profile->last_name);

        return view('users.edit', compact('title','roles','locations','user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CreateProfileRequest $request, $slug)
	{
        $user = User::with(['profile'])->where('slug','=',$slug)->firstOrFail();

        $user = User::doUpdate($user->slug, $request);

        return redirect('users/'.$user->slug.'/edit')->with([
            'alert-level' => 'success',
            'message' => 'User has been successfully updated.'
        ]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
