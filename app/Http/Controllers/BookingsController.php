<?php namespace App\Http\Controllers;

use App\BookedPackage;
use App\Booking;
use App\Commands\BookServicePackage;
use App\Events\AdminSetsPayment;
use App\Location;
use App\Package;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BookingsController extends Controller {

    public function __construct()
    {
        $this->middleware('admin',['only' => [
            'get_confirm','post_confirm','set_payment'
        ]]);

        $this->middleware('costumer',['only' => [
            'prepare','check_date','post_review','get_review','send_review'
        ]]);
    }

    /**
     * Get all bookings.
     *
     * @return \Illuminate\View\View
     */
	public function index()
    {
        if (Auth::user()->isAdmin())
        {
            $bookings = Booking::with(['user','booked_packages','payments','location'])
                ->paginate(10);
        }
        else
        {
            $bookings = Booking::with(['user','booked_packages','payments','location'])
                ->where('user_id','=',Auth::id())
                ->paginate(10);
        }

        $title = 'Bookings';

        return view('bookings.index', compact('title','bookings'));
    }

    /**
     * Compare packages. Only visited by admin.
     *
     * @param $refNumber
     * @return \Illuminate\View\View
     */
    public function get_confirm($refNumber)
    {
        $booking = Booking::with(['user','booked_packages','payments','location'])
            ->where('reference_number','=',$refNumber)
            ->firstOrFail();

        $booked_packages = BookedPackage::getByBooking($booking);
        $packages = BookedPackage::getSameWithBooking($booking);

        $title = 'Ref #: ' . $booking->reference_number;

        return view('bookings.confirm', compact('title','booking','booked_packages','packages'));
    }

    /**
     * Confirm booking details.
     *
     * @param Request $request
     * @param $refNumber
     * @return \Illuminate\Http\RedirectResponse
     */
    public function post_confirm(Request $request, $refNumber)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'status' => 'required'
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->with([
                'alert-level' => 'danger',
                'message' => 'Please provide information on the required fields.'
            ]);
        }

        $booking = Booking::with(['user','booked_packages','payments','location'])
            ->where('reference_number','=',$refNumber)
            ->firstOrFail();

        $booking->price = $request->get('amount') + $booking->location->price;
        $booking->status = $request->get('status');
        $booking->save();

        return redirect('bookings/confirm/' . $refNumber)->with([
            'alert-level' => 'success',
            'message' => 'Booking has been successfully updated.'
        ]);
    }

    /**
     * Set payment to booking.
     *
     * @param Request $request
     * @param $refNumber
     * @return \Illuminate\Http\RedirectResponse
     */
    public function set_payment(Request $request, $refNumber)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric'
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->with([
                'alert-level' => 'danger',
                'message' => 'Please provide information on the required fields.'
            ]);
        }

        $booking = Booking::with(['payments'])
            ->where('reference_number','=',$refNumber)
            ->firstOrFail();

        if ($booking->balance() < $request->get('amount'))
        {
            Session::flash('amount',$request->get('amount'));
            return redirect()->back()->with([
                'alert-level' => 'danger',
                'message' => 'Oops, amount paid is greater than the booking amount.'
            ]);
        }

        $payment = new Payment(['amount' => $request->get('amount')]);

        $booking->payments()->save($payment);

        event(new AdminSetsPayment($booking));

        return redirect('bookings/confirm/' . $refNumber)->with([
            'alert-level' => 'success',
            'message' => 'Payment has been successfully set.'
        ]);
    }

    /**
     * Review / view invoice
     *
     * @param $refNumber
     * @return \Illuminate\View\View
     */
    public function invoice($refNumber)
    {
        $booking = Booking::with(['user','booked_packages','payments','location'])
            ->where('reference_number','=',$refNumber)
            ->firstOrFail();

        $packages = BookedPackage::getByBooking($booking);

        $isMainPackageCustomized = Booking::isMainPackageCustomized($packages);

        $barangay = $booking->barangay;
        $location = $booking->location;
        $event_date = $booking->start_date->format('d-M-Y h:i A');

        $costumer = $booking->user;

        $subtotal = Booking::getSubtotal($packages);
        $total = Booking::getTotal($booking,$subtotal,$isMainPackageCustomized);

        $reference_number = $booking->reference_number;

        $title = '#' . $booking->reference_number;

        return view('invoice.show', compact(
            'title',
            'booking',
            'packages',
            'isMainPackageCustomized',
            'costumer',
            'barangay',
            'location',
            'event_date',
            'subtotal',
            'total',
            'reference_number'
        ));
    }

    /**
     * Prepare costumer booking.
     *
     * @return \Illuminate\View\View
     */
    public function prepare()
    {
        $mainPackages = Package::with(['descriptions'])->main()->get();
        $additionalPackages = Package::with(['descriptions'])->additional()->first();

        $locations = Location::lists('name','id');

        $title = 'Book a Service';

        return view('bookings.prepare', compact('title','mainPackages','additionalPackages','locations'));
    }

    /**
     * Check date's availability.
     *
     * @param Request $request
     * @return array
     */
    public function check_date(Request $request)
    {
        $isDateAvailable = Booking::isDateAvailable($request);

        Session::put([
            'is_date_available' => $isDateAvailable,
            'event_date' => $request->get('event_date'),
            'alert_level' => $isDateAvailable ? 'success' : 'default'
        ]);

        return [
            'is_date_available' => $isDateAvailable,
            'event_date' => $request->get('event_date'),
            'alert_level' => $isDateAvailable ? 'success' : 'default',
            'message' => $isDateAvailable ? 'Date is available.' : 'Date is not available.'
        ];
    }

    /**
     * Set all details from preparation page to session.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function post_review(Request $request)
    {
        Session::put('main_package_id',$request->get('main_package_id'));
        Session::put('additional_package_id',$request->get('additional_package_id'));

        Session::put('main_description_ids',$request->get('main_description_ids'));
        Session::put('additional_description_ids',$request->get('additional_description_ids'));

        Session::put('barangay',$request->get('barangay'));
        Session::put('location_id',$request->get('location_id'));

        return redirect('bookings/review');
    }

    /**
     * Get all review data from session.
     *
     * @return \Illuminate\View\View
     */
    public function get_review()
    {
        $mainPackageID = Session::get('main_package_id');
        $additionalPackageID = Session::get('additional_package_id');

        $mainDescriptionIDs = Session::get('main_description_ids');
        $additionalDescriptionIDs = Session::get('additional_description_ids');

        $barangay = Session::get('barangay');
        $locationID = Session::get('location_id');
        $location = Location::find($locationID);

        $event_date = date('d-M-Y h:i A', strtotime(Session::get('event_date')));

        $mainPackage = Package::with(['descriptions' => function($query) use($mainDescriptionIDs) {
                $query->whereIn('descriptions.id',$mainDescriptionIDs);
            }])
            ->where('id','=',$mainPackageID)
            ->first();

        $packages = [$mainPackage];

        if ($additionalPackageID) {
            $additionalPackage = Package::with(['descriptions' => function ($query) use ($additionalDescriptionIDs) {
                    $query->whereIn('descriptions.id', $additionalDescriptionIDs);
                }])
                ->where('id', '=', $additionalPackageID)
                ->first();

            array_push($packages,$additionalPackage);
        }

        $subtotal = Booking::getSubtotal($packages);
        $total = $subtotal + $location->price;

        $isMainPackageCustomized = Booking::isMainPackageCustomized($packages);
        Session::put('isMainPackageCustomized',$isMainPackageCustomized);

        $costumer = Auth::user();

        $reference_number = '-';

        $title = 'Review Booking';

        return view('bookings.review', compact(
            'title',
            'packages',
            'isMainPackageCustomized',
            'costumer',
            'barangay',
            'location',
            'event_date',
            'subtotal',
            'total',
            'reference_number'
        ));
    }

    /**
     * Send the review to command.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send_review(Request $request)
    {
        $this->dispatch(new BookServicePackage($request));

        return redirect('bookings')->with([
            'alert-level' => 'success',
            'message' => 'You\'ve successfully submitted a booking. Please note that this booking will be reviewed by Administrator.'
        ]);
    }

}
