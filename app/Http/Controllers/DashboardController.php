<?php namespace App\Http\Controllers;

use App\Booking;
use App\Http\Requests;
use App\Package;
use App\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller {

    /**
     * Dashboard
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $unpaidCounter = Booking::getUnpaid();

        $pendingCounter = Booking::with(['payments'])
            ->where('status','=','pending')
            ->roled()
            ->count();

        $activeCounter = Booking::with(['payments'])
            ->where('status','=','active')
            ->roled()
            ->count();

        $packagesCounter = Package::with(['descriptions'])
            ->count();

        $users = User::latest()
            ->with(['profile','bookings','roles'])
            ->where('id','!=',Auth::id())
            ->get();

        $title = 'Dashboard';

        return view('dashboard.index', compact(
            'title',
            'unpaidCounter',
            'pendingCounter',
            'activeCounter',
            'packagesCounter',
            'users'
        ));
    }

}
