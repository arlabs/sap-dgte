<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePackageRequest;
use App\Package;

class PackagesController extends Controller {

    public function __construct()
    {
        $this->middleware('admin',['except' => ['index','show']]);
    }

    /**
     * View list of packages.
     *
     * @return \Illuminate\View\View
     */
	public function index()
    {
        $title = 'Service Packages';

        $Packages = Package::with(['descriptions'])->paginate(8);

        return view('packages.index', compact('title','Packages'));
    }

    /**
     * Returns page to create a package.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $package = null;

        $title = 'Add Package';

        return view('packages.create', compact('title','package'));
    }

    /**
     * Store new package.
     *
     * @param CreatePackageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePackageRequest $request)
    {
        Package::storeOrUpdate($request);

        return redirect('packages')->with([
            'alert-level' => 'success',
            'message' => 'Package has been successfully added.'
        ]);
    }

    /**
     * Show a package based on package ID.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $package = Package::where('id','=',$id)->firstOrFail();

        $title = ucwords($package->name);

        return view('packages.show', compact('title','package'));
    }

    /**
     * Edit a package based on package ID.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $package = Package::where('id','=',$id)->firstOrFail();

        $title = 'Edit Package';

        return view('packages.edit', compact('title','package'));
    }

    /**
     * Update a package based on package ID.
     *
     * @param $id
     * @param CreatePackageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, CreatePackageRequest $request)
    {
        $package = Package::storeOrUpdate($request, $id, 'update');

        return redirect('packages/' . $package->id)->with([
            'alert-level' => 'success',
            'message' => 'Package has been successfully updated.'
        ]);
    }

    /**
     * Deletes a package softly.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $package = Package::where('id','=',$id)->firstOrFail();

        $package->delete();

        return redirect('packages')->with([
            'alert-level' => 'success',
            'message' => 'Package has been successfully deleted.'
        ]);
    }

}
