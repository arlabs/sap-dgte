<?php

Route::get('/', 'PagesController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => 'auth'], function()
{
    Route::get('delete', 'GenericController@delete');
    Route::post('upload', 'GenericController@upload');

    Route::group(['prefix' => 'dashboard'], function()
    {
        Route::get('/', 'DashboardController@index');
    });

    Route::group(['prefix' => 'profile'], function()
    {
        Route::get('/', 'ProfilesController@index');
        Route::get('edit', 'ProfilesController@edit');
        Route::post('update', 'ProfilesController@update');
        Route::post('change_password', 'ProfilesController@change_password');
    });

    Route::group(['prefix' => 'packages'], function()
    {
        Route::get('/', 'PackagesController@index');
        Route::get('create', 'PackagesController@create');
        Route::post('/', 'PackagesController@store');
        Route::get('{id}', 'PackagesController@show');
        Route::get('{id}/edit', 'PackagesController@edit');
        Route::post('{id}', 'PackagesController@update');
        Route::delete('{id}', 'PackagesController@destroy');
    });

    Route::group(['prefix' => 'bookings'], function()
    {
        Route::get('/', 'BookingsController@index');
        Route::get('confirm/{refNumber}', 'BookingsController@get_confirm');
        Route::post('confirm/{refNumber}', 'BookingsController@post_confirm');
        Route::post('set_payment/{refNumber}', 'BookingsController@set_payment');
        Route::get('invoice/{refNumber}', 'BookingsController@invoice');
        Route::get('prepare', 'BookingsController@prepare');
        Route::post('check_date', 'BookingsController@check_date');
        Route::post('review', 'BookingsController@post_review');
        Route::get('review', 'BookingsController@get_review');
        Route::post('send_review', 'BookingsController@send_review');
    });

    Route::group(['prefix' => 'users'], function() {
        Route::get('/', 'UsersController@index');
        Route::get('create', 'UsersController@create');
        Route::post('/', 'UsersController@store');
        Route::get('{slug}/edit', 'UsersController@edit');
        Route::post('{slug}', 'UsersController@update');
    });

    Route::group(['prefix' => 'comments'], function()
    {
        Route::get('/', 'CommentsController@index');
        Route::post('/', 'CommentsController@store');
        Route::post('viewable', 'CommentsController@viewable');
    });

    Route::group(['prefix' => 'locations'], function() {
        Route::get('/', 'LocationsController@index');
        Route::get('create', 'LocationsController@create');
        Route::post('/', 'LocationsController@store');
        Route::get('{id}', 'LocationsController@show');
        Route::post('{id}', 'LocationsController@update');
        Route::delete('{id}', 'LocationsController@destroy');
    });
});