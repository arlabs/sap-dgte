<?php namespace App\Http\Middleware;

use Closure;

class RedirectIfNotCostumer {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if ($request->user()->isCostumer())
        {
            return $next($request);
        }

        return redirect()->back();
	}

}
