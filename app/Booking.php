<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Booking extends Model {

	protected $fillable = [
        'reference_number',
        'user_id',
        'start_date',
        'end_date',
        'barangay',
        'location_id',
        'price',
        'status'
    ];

    protected $dates = ['start_date','end_date'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function booked_packages()
    {
        return $this->hasMany('App\BookedPackage');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function latest_payment()
    {
        return $this->payments()->latest()->take(1);
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    /**
     * Checks date availability.
     *
     * @param Request $request
     * @return bool
     */
    public static function isDateAvailable(Request $request)
    {
        $event_date = $request->get('event_date');
        $hours = $request->get('hours');

        $dateString = date('Y-m-d',strtotime($event_date));
        $startDate = new Carbon($event_date);
        $endDate = new Carbon($event_date);
        $endDate->addHours($hours);

        $bookings = Booking::with(['payments'])
            ->where('start_date','like','%'.$dateString.'%')
            ->where('status','=','active')
            ->orWhere(function($query) use($startDate,$endDate) {
                $query->whereBetween('start_date', [$startDate, $endDate]);
                $query->orWhereBetween('end_date', [$startDate, $endDate]);
            })
            ->get();

        return $bookings->count() < 3 ? true : false;
    }

    /**
     * Checks if main package is customized.
     *
     * @param $packages
     * @return bool
     */
    public static function isMainPackageCustomized($packages)
    {
        $countDescFromBooking = 0;
        $countDescFromOrig = 0;
        foreach ($packages as $package) {
            if ($package->category === 'main')
            {
                $countDescFromBooking = $package->descriptions->count();

                $countDescFromOrig = Package::with(['descriptions'])
                    ->find($package->id)
                    ->descriptions
                    ->count();
            }
        }


        if ($countDescFromOrig !== $countDescFromBooking)
        {
            // Has changes on main package selection
            return true;
        }
        else
        {
            // No changes on main package selection
            return false;
        }
    }

    /**
     * Get subtotal payment. If package is not customized.
     *
     * @param $packages
     * @return int
     */
    public static function getSubtotal($packages)
    {
        $subtotal = 0;

        foreach ($packages as $package)
        {
            if ($package->category == 'main')
            {
                $subtotal += $package->price;
            }
            else
            {
                foreach ($package->descriptions as $desc)
                {
                    $subtotal += $desc->price;
                }
            }
        }

        return $subtotal;
    }

    /**
     * Get total payment.
     *
     * @param $booking
     * @param $subtotal
     * @param $isMainPackageCustomized
     * @return mixed
     */
    public static function getTotal($booking,$subtotal,$isMainPackageCustomized)
    {
        if ($isMainPackageCustomized)
        {
            $total = $booking->price;
        }
        else
        {
            $total = $subtotal + $booking->location->price;
        }

        return $total;
    }

    /**
     * Sets reference number.
     */
    public function setReferenceNumberAttribute()
    {
        $count = Booking::all()->count();

        $userID = str_pad(($count + 1),5,0,STR_PAD_LEFT);

        $this->attributes['reference_number'] = $userID . '-' . rand(1000,9999);
    }

    /**
     * Returns status level.
     *
     * @param $for
     * @return string
     */
    public function status_level($for)
    {
        if ($for == 'booking')
        {
            if ($this->status === 'pending')
            {
                $level = 'warning';
            }
            elseif ($this->status === 'active')
            {
                $level = 'success';
            }
            else
            {
                $level = 'danger';
            }
        }
        elseif ($for == 'payment')
        {
            if ($this->payment_status() == 'paid') {
                $level = 'success';
            }
            else {
                $level = 'warning';
            }
        }
        else
        {
            $level = '';
        }

        return $level;
    }

    /**
     * Returns balance amount.
     *
     * @return mixed
     */
    public function balance()
    {
        $amount = 0;

        if ($this->payments->count())
        {
            foreach ($this->payments as $payment)
            {
                $amount += $payment->amount;
            }
        }

        return $this->price - $amount;
    }

    /**
     * Returns payment status.
     *
     * @return string
     */
    public function payment_status()
    {
        return ($this->balance() == 0 && $this->price > 0) ? 'paid' : 'unpaid';
    }

    /**
     * Returns unpaid costumers.
     *
     * @return int
     */
    public static function getUnpaid()
    {
        $counter = 0;

        $bookings = Booking::roled()->get();

        foreach ($bookings as $booking) {
            if ($booking->payment_status() === 'unpaid')
            {
                $counter++;
            }
        }

        return $counter;
    }

    public function scopeRoled($query)
    {
        if (Auth::user()->isCostumer())
        {
            $query->where('user_id','=',Auth::id());
        }
    }

}
