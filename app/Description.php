<?php namespace App;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

class Description extends Model
{
    protected $fillable = ['content','price'];

    public function packages()
    {
        return $this->morphedByMany('App\Package','descriptionable');
    }

    public function bookings()
    {
        return $this->morphedByMany('App\Booking','descriptionable');
    }

    /**
     * Creates description if it does not exist.
     *
     * @param Request $request
     * @return array
     */
    public static function createIfNotExist(Request $request)
    {
        $description = $request->get('description');
        $price = $request->get('desc_price');

        $descIDs = [];
        for ($i = 0; $i < count($description); $i++)
        {
            $desc = Description::firstOrNew(['content' => $description[$i]]);
            $desc->content = $description[$i];
            $desc->price = $price[$i];
            $desc->save();

            $descIDs[] = $desc->id;
        }

        return $descIDs;
    }

} 