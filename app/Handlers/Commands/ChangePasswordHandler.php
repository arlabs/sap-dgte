<?php namespace App\Handlers\Commands;

use App\Commands\ChangePassword;

use App\Events\UserHasChangedPassword;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class ChangePasswordHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the command.
	 *
	 * @param  ChangePassword  $command
	 * @return void
	 */
	public function handle(ChangePassword $command)
	{
        $user = User::with(['profile'])->where('id','=',Auth::id())->first();

        $user->password = bcrypt($command->new_password);
        $user->save();

        event(new UserHasChangedPassword($user));
	}

}
