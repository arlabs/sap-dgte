<?php namespace App\Handlers\Commands;

use App\BookedPackage;
use App\Booking;
use App\Commands\BookServicePackage;
use App\Events\CostumerHasBookedPackage;
use App\Package;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BookServicePackageHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the command.
	 *
	 * @param  BookServicePackage  $command
	 * @return void
	 */
	public function handle(BookServicePackage $command)
	{
        $package = Package::find($command->main_package_id);

        $startDate = new Carbon($command->event_date);
        $endDate = new Carbon($command->event_date);
        $endDate->addHours($package->hours);

        $total = Session::get('isMainPackageCustomized') ? 0 : $command->total;

        $booking = Booking::create([
            'reference_number' => '',
            'user_id' => Auth::id(),
            'start_date' => $startDate->format('Y-m-d H:i:s'),
            'end_date' => $endDate->format('Y-m-d H:i:s'),
            'barangay' => $command->barangay,
            'location_id' => $command->location_id,
            'price' => $total
        ]);

        $this->bookPackage($booking, $command->main_package_id, $command->main_description_ids);

        if ($command->hasAdditional)
        {
            $this->bookPackage($booking, $command->additional_package_id, $command->additional_description_ids);
        }

        // Die all sessions
        Session::forget([
            'main_package_id',
            'additional_package_id',
            'main_description_ids',
            'additional_description_ids',
            'location_id',
            'is_date_available',
            'event_date',
            'alert_level',
            'isMainPackageCustomized'
        ]);

        event(new CostumerHasBookedPackage($booking));
	}

    private function bookPackage(Booking $booking, $packageID, $descIDs)
    {
        $bookedPackage = new BookedPackage(['package_id' => $packageID]);
        $booking->booked_packages()->save($bookedPackage);

        $bookedPackage->descriptions()->sync($descIDs);
    }

}
