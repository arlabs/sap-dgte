<?php namespace App\Handlers\Events;

use App\Events\CostumerHasBookedPackage;

use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class EmailBookingNotification implements ShouldBeQueued {

    private $mailer;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  CostumerHasBookedPackage  $event
	 * @return void
	 */
	public function handle(CostumerHasBookedPackage $event)
	{
        $user = $event->booking->user;

        $data = [
            'name' => ucwords($user->profile->first_name . ' ' . $user->profile->last_name),
            'booking' => $event->booking
        ];

        $this->mailer->queue('emails.booking-notification', $data, function($message) use($user)
        {
            $message->to($user->email, ucwords($user->profile->first_name.' '.$user->profile->last_name))
                ->subject('Booking Notification [Super Angels Balloons]');
        });
	}

}
