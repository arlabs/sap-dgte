<?php namespace App\Handlers\Events;

use App\Events\UserHasChangedPassword;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class EmailNewPasswordNotification implements ShouldBeQueued {

    private $mailer;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserHasChangedPassword  $event
	 * @return void
	 */
	public function handle(UserHasChangedPassword $event)
	{
        $user = $event->user;

        $data = [
            'name' => ucwords($user->profile->first_name . ' ' . $user->profile->last_name)
        ];

        $this->mailer->queue('emails.change-password', $data, function($message) use($user)
        {
            $message->to($user->email, ucwords($user->profile->first_name.' '.$user->profile->last_name))
                ->subject('Change Password [Super Angels Balloons]');
        });
	}

}
