<?php namespace App\Handlers\Events;

use App\Events\AdminSetsPayment;

use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class EmailInvoice implements ShouldBeQueued {

    protected $mailer;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  AdminSetsPayment  $event
	 * @return void
	 */
	public function handle(AdminSetsPayment $event)
	{
        $user = $event->booking->user;
        $withLatestPayment = $event->booking->with(['latest_payment'])->first();

        $data = [
            'name' => ucwords($user->profile->first_name . ' ' . $user->profile->last_name),
            'booking' => $event->booking,
            'latest_payment' => $withLatestPayment->latest_payment
        ];

        $this->mailer->queue('emails.invoice', $data, function($message) use ( $user )
        {
            $message->to($user->email, ucwords($user->profile->first_name.' '.$user->profile->last_name))
                ->subject('Invoice [Super Angels Balloons]');
        });
	}

}
