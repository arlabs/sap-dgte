<?php namespace App;

use App\Http\Requests\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email', 'password', 'slug'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role','role_user');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Updates user details.
     *
     * @param $slug
     * @param Request $request
     * @return Model|null|static
     */
    public static function doUpdate($slug, Request $request)
    {
        $user = User::with(['profile','roles'])->where('slug','=',$slug)->first();

        $user->profile->first_name = $request->get('first_name');
        $user->profile->last_name = $request->get('last_name');
        $user->profile->mobile = $request->get('mobile');
        $user->profile->barangay = $request->get('barangay');
        $user->profile->location_id = $request->get('location_id');
        $user->profile->save();

        $user->slug = Str::slug($user->id.' '.$user->profile->first_name.' '.$user->profile->last_name);
        $user->save();

        return $user;
    }

    /**
     * Checks if user role is Admin.
     *
     * @return bool
     */
    public function isAdmin()
    {
        $roles = $this->roles->lists('name');

        return !! in_array('admin', $roles);
    }

    /**
     * Checks if user role is costumer.
     *
     * @return bool
     */
    public function isCostumer()
    {
        $roles = $this->roles->lists('name');

        return !! in_array('costumer', $roles);
    }

}
