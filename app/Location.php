<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','km','price'];

    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function profiles()
    {
        return $this->hasMany('App\Profile');
    }

} 