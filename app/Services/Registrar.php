<?php namespace App\Services;

use App\Events\UserHasRegistered;
use App\Profile;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'mobile' => 'required|numeric',
            'location_id' => 'required|numeric',
            'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		$user = User::create([
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);

        // Slug
        $user->slug = Str::slug($user->id.' '.$data['first_name'].' '.$data['last_name']);
        $user->save();

        $profile = new Profile([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'mobile' => $data['mobile'],
            'barangay' => $data['barangay'],
            'location_id' => $data['location_id'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        $user->profile()->save($profile);

        $user->roles()->attach(2);

        // Email welcome notification
        event(new UserHasRegistered($user));

        return $user;
	}

}
