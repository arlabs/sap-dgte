<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'App\Events\UserHasChangedPassword' => [
			'App\Handlers\Events\EmailNewPasswordNotification',
		],
        'App\Events\UserHasRegistered' => [
            'App\Handlers\Events\EmailWelcomeNotification'
        ],
        'App\Events\CostumerHasBookedPackage' => [
            'App\Handlers\Events\EmailBookingNotification'
        ],
        'App\Events\AdminSetsPayment' => [
            'App\Handlers\Events\EmailInvoice'
        ]
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
