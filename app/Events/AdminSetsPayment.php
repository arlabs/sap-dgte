<?php namespace App\Events;

use App\Booking;
use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class AdminSetsPayment extends Event {

	use SerializesModels;

    public $booking;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(Booking $booking)
	{
		$this->booking = $booking;
	}

}
