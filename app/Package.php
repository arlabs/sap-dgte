<?php namespace App;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model {

    use SoftDeletes;

	protected $fillable = [
        'category',
        'name',
        'hours',
        'price',
        'image'
    ];

    public function descriptions()
    {
        return $this->morphToMany('App\Description','descriptionable');
    }

    public function booked_packages()
    {
        return $this->hasMany('App\BookedPackage');
    }

    /**
     * Filters package as main.
     *
     * @param $query
     */
    public function scopeMain($query)
    {
        $query->where('category','=','main');
    }

    /**
     * Filters package as additional.
     *
     * @param $query
     */
    public function scopeAdditional($query)
    {
        $query->where('category','=','additional');
    }

    /**
     * Stores / Updates package.
     *
     * @param Request $request
     * @param null $id
     * @param string $type
     * @return Model|static
     */
    public static function storeOrUpdate(Request $request, $id = null, $type = 'store')
    {
        $descriptionIDs = Description::createIfNotExist($request);

        if ($type === 'store') {
            $package = Package::create(
                [
                    'category' => $request->get('category'),
                    'name' => $request->get('name'),
                    'price' => $request->get('price')
                ]
            );
        }
        else
        {
            $package = Package::with(['descriptions'])->where('id','=',$id)->firstOrFail();

            $package->category = $request->get('category');
            $package->name = $request->get('name');
            $package->price = $request->get('price');
            $package->save();
        }

        $package->descriptions()->sync($descriptionIDs);

        return $package;
    }

}
