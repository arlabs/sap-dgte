<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookedPackage extends Model
{

    protected $fillable = ['booking_id','package_id'];

    public function descriptions()
    {
        return $this->morphToMany('App\Description','descriptionable');
    }

    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    /**
     * Get packages by booking.
     *
     * @param $booking
     * @return array
     */
    public static function getByBooking($booking)
    {
        $packages = [];

        foreach($booking->booked_packages as $booked)
        {
            $packages[] = Package::with(['descriptions' => function($query) use($booked) {
                $query->whereIn('descriptions.id',$booked->descriptions->lists('id'));
            }])
                ->where('id','=',$booked->package->id)
                ->first();
        }

        return $packages;
    }

    /**
     * Get packages same with booking.
     *
     * @param $booking
     * @return array
     */
    public static function getSameWithBooking($booking)
    {
        $packages = [];

        foreach ($booking->booked_packages as $booked)
        {
            $packages[] = $booked->package;
        }

        return $packages;
    }

} 