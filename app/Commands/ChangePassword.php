<?php namespace App\Commands;

use App\Http\Requests\CreatePasswordRequest;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class ChangePassword extends Command implements ShouldBeQueued
{

    public $new_password;

    /**
     * Create a new command instance.
     *
     * @param CreatePasswordRequest $request
     */
	public function __construct(CreatePasswordRequest $request)
	{
		$this->new_password = $request->get('new_password');
	}

}
