<?php namespace App\Commands;

use Illuminate\Http\Request;

class BookServicePackage extends Command {

    public $event_date;

    public $main_package_id;
    public $main_description_ids;

    public $additional_package_id;
    public $additional_description_ids;
    public $hasAdditional = false;

    public $location_id;
    public $barangay;
    public $total;

    /**
     * Create a new command instance.
     *
     * @param Request $request
     */
	public function __construct(Request $request)
	{
		$this->event_date = $request->get('event_date');

		$this->main_package_id = $request->get('main_package_id');
		$this->main_description_ids = $request->get('main_description_ids');

        if ($request->has('additional_package_id'))
        {
            $this->hasAdditional = true;
            $this->additional_package_id = $request->get('additional_package_id');
            $this->additional_description_ids = $request->get('additional_description_ids');
        }

        $this->barangay = $request->get('barangay');
        $this->location_id = $request->get('location_id');
        $this->total = $request->get('total');
	}

}
