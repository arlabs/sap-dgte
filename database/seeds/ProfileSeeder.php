<?php

use App\Profile;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{

    public function run()
    {
        DB::table('profiles')->delete();
        $profiles = [
            [
                'user_id' => 1,
                'first_name' => 'John',
                'last_name' => 'Doe',
                'mobile' => '09283728374',
                'barangay' => 'auckland',
                'location_id' => 2,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'user_id' => 2,
                'first_name' => 'Xen',
                'last_name' => 'Nex',
                'mobile' => '09262140562',
                'barangay' => 'washington',
                'location_id' => 5,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        Profile::insert($profiles);
    }

} 