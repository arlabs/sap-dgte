<?php

use App\Location;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{

    public function run()
    {
        DB::table('locations')->delete();

        $locations = [
            [
                'name' => 'Bacong',
                'km' => 8,
                'price' => 1000,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Dauin',
                'km' => 9,
                'price' => 1100,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Bais',
                'km' => 2,
                'price' => 1200,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'Tanjay',
                'km' => 14,
                'price' => 1300,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
            ,[
                'name' => 'Jimalalud',
                'km' => 21,
                'price' => 1400,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        Location::insert($locations);
    }

} 