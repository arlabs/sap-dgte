<?php

use App\Description;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DescriptionSeeder extends Seeder
{

    public function run()
    {
        DB::table('descriptions')->delete();

        $descriptions = [
            [
                'content' => '1 clown for 1 hour',
                'price' => 1500,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'content' => '1 clown for 1 hour',
                'price' => 1200,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'content' => 'Test Description',
                'price' => 1600,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'content' => 'Choose me, I\'m a description',
                'price' => 1300,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'content' => 'I am a test',
                'price' => 1900,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        Description::insert($descriptions);
    }

} 