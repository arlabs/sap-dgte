<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{

    public function run()
    {
        DB::table('roles')->delete();

        $roles = [
            [
                'name' => 'admin',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'name' => 'costumer',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ];

        Role::insert($roles);

        // Admin Role
        $user = User::find(1);
        $user->roles()->attach(1);

        $user = User::find(2);
        $user->roles()->attach(2);
    }

} 