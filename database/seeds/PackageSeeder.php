<?php

use App\Package;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackageSeeder extends Seeder
{

    public function run()
    {
        DB::table('packages')->delete();

        $packages = [
            [
                'category' => 'main',
                'name' => 'Package 1',
                'hours' => 1,
                'price' => 2200,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'category' => 'main',
                'name' => 'Package 2',
                'hours' => 2,
                'price' => 1200,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'category' => 'main',
                'name' => 'Package 3',
                'hours' => 3,
                'price' => 1500,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ],
            [
                'category' => 'additional',
                'name' => 'Other Services',
                'hours' => 1,
                'price' => 2300,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
            ]
        ];

        Package::insert($packages);

        $packages = Package::with(['descriptions'])->whereIn('id',[1,2,3,4])->get();

        foreach ($packages as $package) {
            $rand = [];
            for ($i=1; $i<=5; $i++)
            {
                $rand[] = rand(1,5);
            }
            $package->descriptions()->sync($rand);
        }

    }

} 