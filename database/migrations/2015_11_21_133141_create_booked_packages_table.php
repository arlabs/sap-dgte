<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateBookedPackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booked_packages', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('booking_id')->unsigned()->index();
			$table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');

            $table->integer('package_id')->unsigned()->index();
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('booked_packages');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
