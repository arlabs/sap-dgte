<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDescriptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('descriptions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->text('content');
            $table->float('price');
			$table->timestamps();
		});

        Schema::create('descriptionables', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('description_id')->unsigned()->index();
            $table->foreign('description_id')->references('id')->on('descriptions')->onDelete('cascade');

            $table->integer('descriptionable_id');
            $table->string('descriptionable_type');
            $table->text('content');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::drop('descriptions');
		Schema::drop('descriptionables');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
